package com.octasolutions.shynapp.util;

import android.content.Context;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.ui.view.LoginActivity;

/**
 * @author HANZALA
 */
public class AuthUtils  {

    private AuthUtils() {
        // private constructor
    }

    public static void logout(DataManager dataManager) {
        dataManager.userId().set(0L);
    }

    public static void openLoginActivity(Context context) {
        LoginActivity.start(context);
    }
}
