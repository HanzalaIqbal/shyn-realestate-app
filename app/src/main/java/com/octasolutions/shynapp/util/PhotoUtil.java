package com.octasolutions.shynapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;

import com.octasolutions.shynapp.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

/**
 * @author Mike
 */
public class PhotoUtil {
    public static final int REQUEST_CODE_TAKE_PHOTO = CommonUtils.getNextInt();
    public static final int REQUEST_CODE_PICK_PHOTO = CommonUtils.getNextInt();

    private PhotoUtil() {
    }

    public static void openGallery(Activity activity) {
        final Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(
                Intent.createChooser(intent, activity.getString(R.string.select_picture)), REQUEST_CODE_PICK_PHOTO
        );
    }

    public static void openGallery(Fragment fragment) {
        final Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        fragment.startActivityForResult(
                Intent.createChooser(intent, fragment.getString(R.string.select_picture)), REQUEST_CODE_PICK_PHOTO
        );
    }

    public static File openCamera(Activity activity) {
        File photoFile = null;
        final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            try {
                photoFile = createTempImageFile(activity);
            } catch (IOException ex) {
                Timber.e(ex, "Unable to create file for storing captured image");
            }
            if (photoFile != null) {
                final Uri photoURI = FileProvider.getUriForFile(activity,
                        "com.octasolutions.shynapp.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(takePictureIntent, REQUEST_CODE_TAKE_PHOTO);
            }
        } else {
            Timber.e("Unable to startForResult camera because there is no app for it");
        }

        return photoFile;
    }

    public static File openCamera(Fragment fragment) {
        File photoFile = null;
        final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
            try {
                photoFile = createTempImageFile(fragment.getActivity());
            } catch (IOException ex) {
                Timber.e(ex, "Unable to create file for storing captured image");
            }
            if (photoFile != null) {
                final Uri photoURI = FileProvider.getUriForFile(fragment.getActivity(),
                        "com.octasolutions.shynapp.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                fragment.startActivityForResult(takePictureIntent, REQUEST_CODE_TAKE_PHOTO);
            }
        } else {
            Timber.e("Unable to startForResult camera because there is no app for it");
        }

        return photoFile;
    }

    private static File createTempImageFile(Activity activity) throws IOException {
        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        final String imageFileName = "SHYN_ANDROID" + timeStamp + "JPEG";
        final File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
