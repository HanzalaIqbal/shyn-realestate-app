package com.octasolutions.shynapp.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.octasolutions.shynapp.R;

import timber.log.Timber;

/**
 * @author Mike
 */
public class PermissionUtil {
    public static final int PERMISSION_REQUEST_CODE_CAMERA = CommonUtils.getNextInt();
    public static final int PERMISSION_REQUEST_CODE_STORAGE = CommonUtils.getNextInt();

    private PermissionUtil() {
    }

    public static boolean isPermissionGranted(@NonNull Context context, @NonNull String permission) {
        try {
            return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        } catch (Throwable t) {
            Timber.e(t, "Failed to check permission %s", permission);
            return false;
        }
    }

    public static void showRationaleDialog(@NonNull Context context, @StringRes int rationale, RationaleCallback callback) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.permission_request)
                .setMessage(rationale)
                .setPositiveButton(R.string.btn_ok, (dialog, which) -> {
                    if (callback != null) {
                        callback.rationaleButtonClick(true);
                    }
                })
                .setNegativeButton(R.string.btn_cancel, (dialog, which) -> {
                    if (callback != null) {
                        callback.rationaleButtonClick(false);
                    }
                })
                .show();
    }

    public interface RationaleCallback {
        void rationaleButtonClick(boolean isPositive);
    }

    public enum Permission {

        CAMERA(
                Manifest.permission.CAMERA,
                PERMISSION_REQUEST_CODE_CAMERA,
                R.string.permission_camera_rationale
        ),
        STORAGE(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                PERMISSION_REQUEST_CODE_CAMERA,
                R.string.permission_storage_rationale
        );

        private final String mPermission;
        private final int mRequestCode;
        @StringRes
        private final int mRationale;

        Permission(String permission, int requestCode, @StringRes int rationale) {
            mPermission = permission;
            mRationale = rationale;
            mRequestCode = requestCode;
        }

        public String getPermission() {
            return mPermission;
        }

        public int getRequestCode() {
            return mRequestCode;
        }

        @StringRes
        public int getRationale() {
            return mRationale;
        }
    }
}
