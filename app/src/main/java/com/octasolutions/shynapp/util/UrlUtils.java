package com.octasolutions.shynapp.util;

import android.app.Activity;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;

import com.octasolutions.shynapp.R;


/**
 * @author Mike
 */
public class UrlUtils {

    private UrlUtils() {}

    public static void openUrl(Activity activity, String url) {
        final CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        final CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(activity, Uri.parse(url));
    }

    public static String getPictureUrl(String pictureURL) {
        return "http://demo.octasolutions.pk/real_estate/images/" + pictureURL;
    }
}
