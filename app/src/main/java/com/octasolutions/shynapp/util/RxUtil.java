package com.octasolutions.shynapp.util;

import android.content.Context;
import android.support.annotation.NonNull;

import com.octasolutions.shynapp.ShynApplication;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class RxUtil {

    public static void unsubscribe(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @NonNull
    public static <T> Observable<T> async(Observable<T> observable) {
        return observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public static void post(Context context, Object event) {
        ShynApplication.get(context).getComponent().eventBus().post(event);
    }

    public static <T> void subscribeEvent(RxEventBus eventBus, CompositeSubscription subscription,
                                          Class<T> clazz, Action1<? super T> onNext) {
        subscription.add(eventBus
                .filteredObservable(clazz)
                .subscribe(
                        onNext,
                        throwable -> {
                            Timber.tag(RxUtil.class.getSimpleName()).e(throwable, "RxEventBus error");
                        }
                ));
    }
}
