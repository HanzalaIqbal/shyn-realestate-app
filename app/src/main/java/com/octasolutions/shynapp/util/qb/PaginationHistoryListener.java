package com.octasolutions.shynapp.util.qb;

public interface PaginationHistoryListener {
    void downloadMore();
}
