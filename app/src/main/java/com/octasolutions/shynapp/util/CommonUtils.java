package com.octasolutions.shynapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.PostObject;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author HANZALA
 */
public class CommonUtils {

    public static final AtomicInteger seed = new AtomicInteger();

    /**
     * Ensure this class is only used as a utility.
     */
    private CommonUtils() {
        throw new AssertionError();
    }

    /**
     * This method is used to hide a keyboard
     */
    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static int getNextInt() {
        return seed.incrementAndGet();
    }

    public static Drawable getDrawable(Resources resources, int resourceId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            return resources.getDrawable(resourceId);
        } else {
            return resources.getDrawable(resourceId, null);
        }
    }

    public static int getColor(Context context, int resourceId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return context.getResources().getColor(resourceId);
        } else {
            return context.getResources().getColor(resourceId, context.getTheme());
        }
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        final int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static boolean hasPermission(Context context, String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static boolean validateField(EditText editText, String msg, int limit) {
        if (editText.getText().toString().length() < limit) {
//            editText.setError(msg + "");
//            editText.requestFocus();
            return false;
        } else {
//            editText.setError(null);
            return true;
        }
    }

    public static void sharePost(Context context, PostObject post) {
        String address = String.format(context.getString(R.string.post_address),
                post.getHouseNo(), post.getStreetNo(), post.getCityName());
        String imageURL = UrlUtils.getPictureUrl(post.getImg1());
        String postData = String.format(context.getString(R.string.share_post_string),
                post.getAskPrice(), post.getBeds(), post.getBaths(), post.getEstARV(),
                post.getEstRepCost(), post.getSqFeet(), address, imageURL);

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, postData);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

}