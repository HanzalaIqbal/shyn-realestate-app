package com.octasolutions.shynapp.ui.mvpview;

import com.octasolutions.shynapp.data.model.PurchasedCityObject;
import com.octasolutions.shynapp.data.model.StateObject;
import com.octasolutions.shynapp.ui.base.MvpView;

import java.util.ArrayList;

/**
 * @author HANZALA
 */

public interface NewPostMvpView extends MvpView {
    void onAllStatesResult(ArrayList<StateObject> states);

    void onPurchasedCitiesResult(ArrayList<PurchasedCityObject> cities);

    void onPostSuccess();
}
