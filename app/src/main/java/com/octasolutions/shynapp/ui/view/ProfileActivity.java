package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.octasolutions.shynapp.Henson;
import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.data.model.UserObject;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.ProfileMvpView;
import com.octasolutions.shynapp.ui.presenter.ProfilePresenter;
import com.octasolutions.shynapp.util.CircleTransform;
import com.octasolutions.shynapp.util.UrlUtils;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author HANZALA
 */
public class ProfileActivity extends BaseActivity implements ProfileMvpView {

    @Inject
    ProfilePresenter mPresenter;

    @BindView(R.id.iv_photo)
    ImageView mProfilePhoto;
    @BindView(R.id.tv_followers)
    TextView mFollowers;
    @BindView(R.id.tv_following)
    TextView mFollowing;
    @BindView(R.id.tv_name)
    TextView mName;
    @BindView(R.id.tv_address)
    TextView mProfileLocation;
    @BindView(R.id.tv_web_address)
    TextView mWebAddress;
    @BindView(R.id.v_first_post)
    View mFirstPost;

    @InjectExtra
    long mUserId = -1;

    public static void start(Context context, long currentUserId) {
        final Intent intent = Henson.with(context)
                .gotoProfileActivity()
                .mUserId(currentUserId)
                .build();
        context.startActivity(intent);

//        context.startActivity(new Intent(context, ProfileActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initToolbar();
        Dart.inject(this);
        activityComponent().inject(this);
        mPresenter.attachView(this);

        mFirstPost.setVisibility(View.GONE);
        mPresenter.getProfile(mUserId);
        mPresenter.getPosts(mUserId);

    }


    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
        setNavigationIcon(R.drawable.ic_home);
        setActionBarTitle(R.string.ab_profile);
    }


    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_profile;
    }

//    @Override
//    protected void initToolbar() {
//        super.initToolbar();
//        if (mActionBar != null) {
////            mActionBar.setDisplayHomeAsUpEnabled(true);
//        }
////        setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.edit_profile:
                SetProfileActivity.start(this);
                return true;
            case R.id.zip_code_purchase:
                ZipCodePurchaseActivity.start(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.ll_follwers)
    void openFollowersActivity() {
        FollowersActivity.start(this);
    }

    @OnClick(R.id.fab_post)
    void openPostActivity() {
        PostActivity.start(this);
    }

    @OnClick(R.id.ll_follwing)
    void openFollowingActivity() {
        FollowingActivity.start(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void displayProfileInfo(UserObject data) {
        final int size = getResources().getDimensionPixelSize(R.dimen.user_picture_size);
        Picasso.with(this)
                .load(UrlUtils.getPictureUrl(data.getProfilePicture()))
                .placeholder(R.drawable.thumbnail_placeholder)
                .resize(size, size)
                .centerCrop()
                .transform(new CircleTransform())
                .into(mProfilePhoto);
        mFollowers.setText(data.getFollower());
        mFollowing.setText(data.getFollowing());
        mName.setText(String.format("%s %s %s", data.getFirstName(), data.getMiddleName(), data.getLastName()));
        mProfileLocation.setText(data.getHomeAddress());
        mWebAddress.setText(data.getWebAddress());
    }

    @BindView(R.id.iv_post_photo)
    ImageView mPhoto;
    @BindView(R.id.tv_ask_price)
    TextView mAskPrice;
    @BindView(R.id.tv_beds)
    TextView mBeds;
    @BindView(R.id.tv_baths)
    TextView mBaths;
    @BindView(R.id.tv_est_avg)
    TextView mEstAvg;
    @BindView(R.id.tv_est_rep)
    TextView mEstRep;
    @BindView(R.id.tv_sq_feet)
    TextView mSqFeet;
    @BindView(R.id.ll_post_actions)
    LinearLayout mPostActions;
    @BindView(R.id.tv_location)
    TextView mLocation;

    @Override
    public void displayFirstPost(PostObject mPostObject) {
        mFirstPost.setVisibility(View.VISIBLE);
        mPostActions.setVisibility(View.GONE);

        Picasso.with(this)
                .load(UrlUtils.getPictureUrl(mPostObject.getImg1()))
                .centerCrop()
                .fit()
                .into(mPhoto);
        mAskPrice.setText(String.format(this.getString(R.string.cost), mPostObject.getAskPrice()));
        mBeds.setText(String.format(this.getString(R.string.s_beds), mPostObject.getBeds()));
        mBaths.setText(String.format(this.getString(R.string.s_baths), mPostObject.getBaths()));
        mEstAvg.setText(String.format(this.getString(R.string.cost), mPostObject.getEstARV()));
        mEstRep.setText(String.format(this.getString(R.string.cost), mPostObject.getEstRepCost()));
        mSqFeet.setText(String.format(this.getString(R.string.s_sqft), mPostObject.getSqFeet()));
        mLocation.setText(String.format(this.getString(R.string.post_address),
                mPostObject.getHouseNo(), mPostObject.getStreetNo(), mPostObject.getCityName()));
    }

    @OnClick(R.id.btn_posts)
    public void openMyPosts(View view) {
        YourPostsActivity.start(this,mUserId);
    }
}