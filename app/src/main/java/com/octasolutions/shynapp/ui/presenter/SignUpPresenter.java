package com.octasolutions.shynapp.ui.presenter;

import android.text.TextUtils;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.SignupResponse;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.SignUpMvpView;
import com.octasolutions.shynapp.util.CommonUtils;
import com.octasolutions.shynapp.util.RxUtil;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

/**
 * @author HANZALA
 */
public class SignUpPresenter extends BasePresenter<SignUpMvpView> {

    private final DataManager dataManager;
    private CompositeSubscription subscription;

    @Inject
    public SignUpPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void signUpEmail(String firstName, String middleName, String lastName, String dob, String gender, String email, String password) {
        checkViewAttached();
        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(middleName) || TextUtils.isEmpty(lastName) ||
                TextUtils.isEmpty(dob) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            getMvpView().displayToast("All fields must be filled");
            return;
        }
        if (!CommonUtils.isValidEmail(email)) {
            getMvpView().displayToast("Email not valid");
            return;
        }
        if (password.equals("false")) {
            getMvpView().displayToast("Passwords don't match");
            return;
        }
        Timber.w("first name: %s \nmiddle name: %s \nlastname: %s \nDob: %s \ngender: %s\nemail: %s\npasssword: %s",
                firstName, middleName, lastName, dob, gender, email, password);
        subscription.add(
                dataManager.signup(firstName,middleName,lastName,dob,gender,email,password)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleEmailSignupResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );

    }

    private void handleEmailSignupResponse(SignupResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                dataManager.userId().set(Long.valueOf(response.data.getUserId()));
                getMvpView().signupSuccess(response.msg);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }
}