package com.octasolutions.shynapp.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.CityObject;
import com.octasolutions.shynapp.data.model.PromotionObject;
import com.octasolutions.shynapp.data.model.StateObject;
import com.octasolutions.shynapp.data.model.ZipCodeObject;
import com.octasolutions.shynapp.event.ZipSelectEvent;
import com.octasolutions.shynapp.ui.adapter.ZipCodesAdapter;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.ZipCodeMvpView;
import com.octasolutions.shynapp.ui.presenter.ZipCodePresenter;
import com.octasolutions.shynapp.util.TimeUtils;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * @author HANZALA
 */
public class ZipCodePurchaseActivity extends BaseActivity implements ZipCodeMvpView {

    @Inject
    ZipCodePresenter mPresenter;

    @BindView(R.id.spinner_state)
    Spinner mStates;
    @BindView(R.id.spinner_city)
    Spinner mCities;
    @BindView(R.id.tv_available_count)
    TextView mAvailableCount;
    @BindView(R.id.tv_selected_count)
    TextView mSelectedCount;
    @BindView(R.id.rv_available_zips)
    RecyclerView mAvailableZips;
    @BindView(R.id.rv_selected_zips)
    RecyclerView mSelectedZips;

    //payment views
    @BindView(R.id.tv_month_mul)
    TextView mMonthMul;
    @BindView(R.id.tv_year_mul)
    TextView mYearMul;
    @BindView(R.id.tv_month_discount)
    TextView mMonthDiscount;
    @BindView(R.id.tv_year_discount)
    TextView mYearDiscount;
    @BindView(R.id.tv_month_total)
    TextView mMonthTotal;
    @BindView(R.id.tv_year_total)
    TextView mYearTotal;
    @BindView(R.id.tv_month_select)
    TextView mMonthSelect;
    @BindView(R.id.tv_year_select)
    TextView mYearSelect;

    boolean isYear = false;

    ZipCodesAdapter mAvailableZipCodesAdapter;
    ZipCodesAdapter mSelectedZipCodesAdapter;

    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
//            paypal client ID
            .clientId("AUqE3EdPVbq1fmq7pmcQkZohT2_-MD5txO1-Fqc_C-ah-Zsar_iDLAtw3nyBTNbq9-INpbXKzlk2ruU7");


    private StateObject mState;
    private CityObject mCity;
    private PromotionObject mPromotion;
    private String mPaymentID;

    public static void start(Context context) {
        context.startActivity(new Intent(context, ZipCodePurchaseActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initToolbar();
        activityComponent().inject(this);
        mPresenter.attachView(this);

//        paypal service
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        mPresenter.getAllStates();
        mPresenter.getPromotions();

        mAvailableZipCodesAdapter = new ZipCodesAdapter();
        mSelectedZipCodesAdapter = new ZipCodesAdapter();
        mAvailableZips.setLayoutManager(new LinearLayoutManager(this));
        mAvailableZips.setAdapter(mAvailableZipCodesAdapter);
        mSelectedZips.setLayoutManager(new LinearLayoutManager(this));
        mSelectedZips.setAdapter(mSelectedZipCodesAdapter);

        updateAvailableZipCodeView(new ArrayList<>());
        updateSelectedZipCodeView();
    }

    ArrayList<ZipCodeObject> mSelectedCodes = new ArrayList<>();

    void updateAvailableZipCodeView(ArrayList<ZipCodeObject> available) {
        mAvailableCount.setText(String.format(getString(R.string.availabe_zip_codes), available.size()));
        mAvailableZipCodesAdapter.setData(available, ZipCodesAdapter.ZipCodeType.AVAILABLE);
    }

    void updateSelectedZipCodeView() {
        mSelectedCount.setText(String.format(getString(R.string.selected_zip_codes), mSelectedCodes.size()));
        mSelectedZipCodesAdapter.setData(mSelectedCodes, ZipCodesAdapter.ZipCodeType.SELECTED);
        if (mPromotion == null)
            return;
        updateMonthlyData(mSelectedCodes.size());
        updateYearlyData(mSelectedCodes.size());
    }

    protected void subscribe() {
        super.subscribe();
        subscribeEvent(ZipSelectEvent.class, this::onZipSelectEvent);
    }

    private void onZipSelectEvent(ZipSelectEvent event) {
        if (event.getType() == ZipCodesAdapter.ZipCodeType.AVAILABLE) {
            if (event.getZipCode().getStatus() == ZipCodeObject.Purchased.PURCHASED) {
                displayToast("Zip Code Already " + event.getZipCode().getStatus());
                return;
            }
            for (ZipCodeObject object : mSelectedCodes) {
                if (object.getZipCodeId() == event.getZipCode().getZipCodeId())
                    return;
            }
            mSelectedCodes.add(event.getZipCode());
        } else if (event.getType() == ZipCodesAdapter.ZipCodeType.SELECTED && mSelectedCodes.contains(event.getZipCode())) {
            mSelectedCodes.remove(event.getZipCode());
        }
        updateSelectedZipCodeView();
    }

    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_zip_code_purchase;
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        findViewById(R.id.action_logo).setVisibility(View.GONE);
        setNavigationIcon(R.drawable.ic_home);
        setActionBarTitle(R.string.ab_zip_code_purchase);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
        mPresenter.detachView();
    }

    @OnClick(R.id.submit_pay)
    public void openPaymentConfirmActivity() {
        if (totalMonthlyPriceWithDiscount < 1 || totalYearlyPriceWithDiscount < 1 || mSelectedCodes.size() == 0) {
            displayToast("Please select zip code first");
            return;
        }

//        PaymentConfirmActivity.start(this);
        // PAYMENT_INTENT_SALE will cause the payment to complete immediately.
        // Change PAYMENT_INTENT_SALE to
        //   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
        //   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
        //     later via calls from your server.

        PayPalPayment payment = new PayPalPayment(new BigDecimal(isYear ? totalYearlyPriceWithDiscount : totalMonthlyPriceWithDiscount), "USD", String.format("%d Zip codes", mSelectedCodes.size()),
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Timber.w("paymentExample: " + confirm.toJSONObject().toString(4));
                    Timber.w("getPayment: " + confirm.getPayment().toString());
                    Timber.w("getProofOfPayment: " + confirm.getProofOfPayment());
                    Timber.w("getCreateTime: " + confirm.getProofOfPayment().getCreateTime());
                    Timber.w("getPaymentId: " + confirm.getProofOfPayment().getPaymentId());
                    mPaymentID = confirm.getProofOfPayment().getPaymentId();
                    Timber.w("getTransactionId: " + confirm.getProofOfPayment().getTransactionId());

                    // TODO: send 'confirm' to your server for verification.
                    StringBuilder sb = new StringBuilder();
                    String separator = ",";
                    for (ZipCodeObject n : mSelectedCodes) {
                        if (sb.length() > 0) sb.append(separator);
                        sb.append(n.getZipCodeId());
                    }
                    Timber.w("Converted Zip codes \n %s", sb.toString());

                    String zipCodeIDs = sb.toString();
                    String paymentDetail = confirm.toJSONObject().toString(4);
                    int yearly = isYear ? 1 : 0;
                    String dateTime = TimeUtils.getCurrentTime();
                    mPresenter.purchaseZipcode(zipCodeIDs, paymentDetail, yearly, dateTime);
                } catch (JSONException e) {
                    Timber.w(e, "paymentExample: " + "an extremely unlikely failure occurred: ");
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Timber.w("paymentExample: " + "The user canceled.");
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Timber.w("paymentExample: " + "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }

    @Override
    public void onAllStatesResult(ArrayList<StateObject> states) {
        states.add(0, new StateObject(getResources().getString(R.string.select_state)));
        if (states.size() == 1)
            return;

        Timber.w("Size %s", states.size());
        ArrayAdapter<StateObject> spinnerArrayAdapter = new ArrayAdapter<StateObject>(this, android.R.layout.simple_spinner_item, states); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStates.setAdapter(spinnerArrayAdapter);
        mStates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    return;
                mPresenter.getCities(states.get(position).getStateId());
                mState = states.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onAllCitiesResult(ArrayList<CityObject> cities) {
        cities.add(0, new CityObject(getResources().getString(R.string.select_city)));
        if (cities.size() == 1)
            return;

        Timber.w("Size %s", cities.size());
        ArrayAdapter<CityObject> spinnerArrayAdapter = new ArrayAdapter<CityObject>(this, android.R.layout.simple_spinner_item, cities); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCities.setAdapter(spinnerArrayAdapter);
        mCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    return;
                mPresenter.getZipCodes(cities.get(position).getCityId());
                mCity = cities.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onZipCodesResult(ArrayList<ZipCodeObject> zipCodes) {
        updateAvailableZipCodeView(zipCodes);
    }

    @Override
    public void onPromotionResult(PromotionObject promotion) {
        mPromotion = promotion;
        updateMonthlyData(0);
        updateYearlyData(0);
    }

    @Override
    public void onPurchaseZipCodesResult(String msg) {
        displayToast(msg);
        PaymentConfirmActivity.start(this,isYear ? totalYearlyPriceWithDiscount : totalMonthlyPriceWithDiscount,mPaymentID);
    }

    double totalMonthlyPriceWithDiscount = 0;
    double totalYearlyPriceWithDiscount = 0;

    void updateMonthlyData(int totalCodes) {
        mMonthMul.setText(String.format(getString(R.string.rate_multiply), String.valueOf(mPromotion.getPriceMonth()), totalCodes));
        mMonthDiscount.setText(String.format(getString(R.string.off_discount), String.valueOf(mPromotion.getDiscount())));

        totalMonthlyPriceWithDiscount = (mPromotion.getPriceMonth() * totalCodes) * ((100 - mPromotion.getDiscount()) / 100);
        mMonthTotal.setText(String.format(getString(R.string.rate_total), totalCodes == 0 ? "0.00" : String.valueOf(totalMonthlyPriceWithDiscount)));
    }

    void updateYearlyData(int totalCodes) {
        mYearMul.setText(String.format(getString(R.string.rate_multiply), String.valueOf(mPromotion.getPriceYear()), totalCodes));
        mYearDiscount.setText(String.format(getString(R.string.off_discount), String.valueOf(mPromotion.getDiscount())));
        totalYearlyPriceWithDiscount = (mPromotion.getPriceYear() * totalCodes) * ((100 - mPromotion.getDiscount()) / 100);
        mYearTotal.setText(String.format(getString(R.string.rate_total), totalCodes == 0 ? "0.00" : String.valueOf(totalYearlyPriceWithDiscount)));
    }

    @OnClick(R.id.tv_month_select)
    public void selectMonth(View view) {
        mMonthSelect.setText(R.string.selected);
        mMonthSelect.setBackgroundColor(ActivityCompat.getColor(this, R.color.colorPrimary));
        mMonthSelect.setTextColor(ActivityCompat.getColor(this, R.color.white));
        mYearSelect.setText(R.string.select);
        mYearSelect.setBackgroundColor(ActivityCompat.getColor(this, R.color.white));
        mYearSelect.setTextColor(ActivityCompat.getColor(this, R.color.colorPrimary));
        isYear = false;
    }

    @OnClick(R.id.tv_year_select)
    public void selectYear(View view) {
        mMonthSelect.setText(R.string.select);
        mMonthSelect.setBackgroundColor(ActivityCompat.getColor(this, R.color.white));
        mMonthSelect.setTextColor(ActivityCompat.getColor(this, R.color.colorPrimary));
        mYearSelect.setText(R.string.selected);
        mYearSelect.setBackgroundColor(ActivityCompat.getColor(this, R.color.colorPrimary));
        mYearSelect.setTextColor(ActivityCompat.getColor(this, R.color.white));
        isYear = true;
    }
}