package com.octasolutions.shynapp.ui.adapter.viewHolder;

import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.event.AddPhotoClickedEvent;
import com.octasolutions.shynapp.util.RxUtil;
import com.octasolutions.shynapp.util.ViewUtil;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.myinnos.awesomeimagepicker.models.Image;

/**
 * @author HANZALA
 */

public class ImageViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_photo)
    ImageView mImage;
    @BindView(R.id.tv_add_photo)
    TextView tvAddPhoto;

    private Image image;
    private int mPosition;

    public ImageViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bind(Image image, int position) {
        this.image = image;
        mPosition = position;
        if (this.image != null) {
            Uri uri = Uri.fromFile(new File(this.image.path));
            ViewUtil.setVisibility(View.INVISIBLE, tvAddPhoto);
            Picasso.with(mImage.getContext())
                    .load(uri)
                    .placeholder(R.drawable.thumbnail_placeholder)
//                    .transform(new CircleTransform())
                    .into(mImage);
        } else {
            ViewUtil.setVisibility(View.VISIBLE, tvAddPhoto);
            mImage.setImageDrawable(ContextCompat.getDrawable(itemView.getContext(), R.color.txt_color_selected));
        }
    }

    @OnClick(R.id.tv_add_photo)
    public void onAddPhotoClicked(View v) {
        RxUtil.post(v.getContext(), new AddPhotoClickedEvent(mPosition));
    }
}
