package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.octasolutions.shynapp.Henson;
import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.ui.adapter.MyPagerAdapter;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.PostDetailMvpView;
import com.octasolutions.shynapp.ui.presenter.PostDetailPresenter;
import com.octasolutions.shynapp.ui.widget.SmartViewPager;
import com.octasolutions.shynapp.util.UrlUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * @author HANZALA
 */
public class PostDetailActivity extends BaseActivity implements PostDetailMvpView {

    @Inject
    PostDetailPresenter mPresenter;

    @BindView(R.id.vp_images)
    SmartViewPager mImageViewPager;
    @BindView(R.id.tabDots)
    TabLayout tabDots;

    @BindView(R.id.tv_ask_price)
    TextView mAskPrice;
    @BindView(R.id.tv_beds)
    TextView mBeds;
    @BindView(R.id.tv_baths)
    TextView mBaths;
    @BindView(R.id.tv_est_avg)
    TextView mEstAvg;
    @BindView(R.id.tv_est_rep)
    TextView mEstRep;
    @BindView(R.id.tv_sq_feet)
    TextView mSqFeet;
    @BindView(R.id.tv_location)
    TextView mLocation;

    @BindView(R.id.tv_price_)
    TextView mPrice;
    @BindView(R.id.tv_beds_)
    TextView mBeds_;
    @BindView(R.id.tv_baths_)
    TextView mBaths_;
    @BindView(R.id.tv_address)
    TextView mAddress_;
    @BindView(R.id.tv_city)
    TextView mCity;
    @BindView(R.id.tv_zip_code)
    TextView mZipCode;


    @InjectExtra
    long mPostId = -1;

    private MyPagerAdapter adapter;

    public static void start(Context context, long postId) {
//        context.startActivity(new Intent(context, PostDetailActivity.class));
        final Intent intent = Henson.with(context)
                .gotoPostDetailActivity()
                .mPostId(postId)
                .build();
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        initToolbar();
        initView();
        mPresenter.attachView(this);
        Dart.inject(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getPostDetails(mPostId);
        Timber.w("Post Id %s", mPostId + "");
    }

    private void initView() {
        tabDots.setupWithViewPager(mImageViewPager, true);
        adapter = new MyPagerAdapter();
        mImageViewPager.setAdapter(adapter);
    }


    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_post_detail;
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        findViewById(R.id.action_logo).setVisibility(View.GONE);
        setNavigationIcon(R.drawable.ic_home);
//        setActionBarTitle(R.string.ab_zip_code_purchase);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }


    @Override
    public void displayPostDetail(PostObject mPostObject) {
        mAskPrice.setText(String.format(getString(R.string.cost), mPostObject.getAskPrice()));
        mPrice.setText(String.format(getString(R.string.cost), mPostObject.getAskPrice()));
        mBeds.setText(String.format(getString(R.string.s_beds), mPostObject.getBeds()));
        mBeds_.setText(String.format(getString(R.string.s_beds), mPostObject.getBeds()));
        mBaths.setText(String.format(getString(R.string.s_baths), mPostObject.getBaths()));
        mBaths_.setText(String.format(getString(R.string.s_baths), mPostObject.getBaths()));
        mEstAvg.setText(String.format(getString(R.string.cost), mPostObject.getEstARV()));
        mEstRep.setText(String.format(getString(R.string.cost), mPostObject.getEstRepCost()));
        mSqFeet.setText(String.format(getString(R.string.s_sqft), mPostObject.getSqFeet()));
        mLocation.setText(String.format(getString(R.string.post_address),
                mPostObject.getHouseNo(), mPostObject.getStreetNo(), mPostObject.getCityName()));
        mAddress_.setText(String.format(getString(R.string.post_address),
                mPostObject.getHouseNo(), mPostObject.getStreetNo(), mPostObject.getCityName()));
        mCity.setText(String.format("%s", mPostObject.getCityName()));
        mZipCode.setText(mPostObject.getZipCode());
        ArrayList<String> images = new ArrayList<>();
        if (mPostObject.getImg1() != null && !mPostObject.getImg1().isEmpty())
            images.add(UrlUtils.getPictureUrl(mPostObject.getImg1()));
        if (mPostObject.getImg2() != null && !mPostObject.getImg2().isEmpty())
            images.add(UrlUtils.getPictureUrl(mPostObject.getImg2()));
        if (mPostObject.getImg3() != null && !mPostObject.getImg3().isEmpty())
            images.add(UrlUtils.getPictureUrl(mPostObject.getImg3()));
        if (mPostObject.getImg4() != null && !mPostObject.getImg4().isEmpty())
            images.add(UrlUtils.getPictureUrl(mPostObject.getImg4()));
        if (mPostObject.getImg5() != null && !mPostObject.getImg5().isEmpty())
            images.add(UrlUtils.getPictureUrl(mPostObject.getImg5()));
        if (images.size() > 0) {
            mPresenter.showImages(images);
        }

    }

    @Override
    public void displayImages(ArrayList<String> img) {
        ArrayList<View> imageViews = new ArrayList<>();
        for (String image : img) {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            Picasso.with(this)
                    .load(image)
                    .centerCrop()
                    .fit()
                    .into(imageView);
            imageViews.add(imageView);
        }
        if (imageViews.size() > 0) {
            adapter.addAllViews(imageViews);
        }
    }
}