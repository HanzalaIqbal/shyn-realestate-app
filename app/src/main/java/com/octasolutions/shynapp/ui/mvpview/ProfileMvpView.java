package com.octasolutions.shynapp.ui.mvpview;

import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.data.model.UserObject;
import com.octasolutions.shynapp.ui.base.MvpView;

/**
 * @author HANZALA
 */

public interface ProfileMvpView extends MvpView {
    void displayProfileInfo(UserObject data);

    void displayFirstPost(PostObject post);
}
