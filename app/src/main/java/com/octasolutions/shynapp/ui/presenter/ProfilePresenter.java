package com.octasolutions.shynapp.ui.presenter;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.remote.response.GetPostsResponse;
import com.octasolutions.shynapp.data.remote.response.GetProfileResponse;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.ProfileMvpView;
import com.octasolutions.shynapp.util.RxUtil;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

/**
 * @author HANZALA
 */

public class ProfilePresenter extends BasePresenter<ProfileMvpView> { private final DataManager dataManager;
    private CompositeSubscription subscription;

    @Inject
    public ProfilePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void getProfile(long mUserId) {
        checkViewAttached();
        subscription.add(
                dataManager.getProfile(mUserId)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleProfileResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );

    }

    private void handleProfileResponse(GetProfileResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().displayProfileInfo(response.data);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }


    public void getPosts(long mUserId) {
        checkViewAttached();
        subscription.add(
                dataManager.getPosts(mUserId)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handlePostsResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handlePostsResponse(GetPostsResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().displayFirstPost(response.data.get(0));
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

}
