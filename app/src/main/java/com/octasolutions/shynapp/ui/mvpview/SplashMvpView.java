package com.octasolutions.shynapp.ui.mvpview;


import com.octasolutions.shynapp.ui.base.MvpView;

public interface SplashMvpView extends MvpView {
    void login();
}
