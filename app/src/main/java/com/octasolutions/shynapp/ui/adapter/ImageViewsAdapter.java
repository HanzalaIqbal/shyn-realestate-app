package com.octasolutions.shynapp.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.adapter.viewHolder.ImageViewHolder;

import java.util.ArrayList;
import java.util.List;

import in.myinnos.awesomeimagepicker.models.Image;

/**
 * @author HANZALA
 */

public class ImageViewsAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    private static final int MAX_SIZE = 5;
    private List<Image> mList;

    public ImageViewsAdapter() {
        mList = new ArrayList<>();
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_imageview, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        holder.bind(mList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setData(@NonNull List<Image> dataList) {
        mList.clear();
        mList.addAll(dataList);
        if (dataList.size() < MAX_SIZE)
            mList.add(null);
        notifyDataSetChanged();
    }
}
