package com.octasolutions.shynapp.ui.mvpview;

import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.ui.base.MvpView;

import java.util.ArrayList;

/**
 * @author HANZALA
 */
public interface FavoriteMvpView extends MvpView {
    void displayFavoritePosts(ArrayList<PostObject> data);

    void likePostById(long userId, long postId, PostObject.LikeStatus liked);
}