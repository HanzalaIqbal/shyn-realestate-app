package com.octasolutions.shynapp.ui.base;

import android.support.annotation.StringRes;

/**
 * @author HANZALA
 */
public interface ErrorMvpView {
    void networkError(Throwable throwable);
    void displayToast(String message);
    void displayToast(@StringRes int message);
}