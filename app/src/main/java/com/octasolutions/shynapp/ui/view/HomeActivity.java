package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.michael.easydialog.EasyDialog;
import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.event.PostLikedEvent;
import com.octasolutions.shynapp.ui.adapter.PostsAdapter;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.HomeMvpView;
import com.octasolutions.shynapp.ui.presenter.HomePresenter;
import com.octasolutions.shynapp.ui.widget.TabsViewPager;
import com.octasolutions.shynapp.util.AuthUtils;
import com.octasolutions.shynapp.util.ViewUtil;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * @author HANZALA
 */
public class HomeActivity extends BaseActivity implements HomeMvpView {

    @Inject
    HomePresenter mPresenter;

    @BindView(R.id.rv_posts)
    RecyclerView mPostsList;

    @BindView(R.id.iv_filter)
    ImageView ivFilter;

    PostsAdapter mAdapter;

    public static void start(Context context) {
        final Intent startIntent = new Intent(context, HomeActivity.class);
        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        initToolbar();

        mPresenter.attachView(this);

        mAdapter = new PostsAdapter();
        mPostsList.setLayoutManager(new LinearLayoutManager(this));
        mPostsList.setAdapter(mAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getAllProperties();
        ViewUtil.hideKeyboard(this);
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
        setNavigationIcon(R.drawable.ic_profile_dark);
    }


    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_home;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ProfileActivity.start(this,mPresenter.getCurrentUserId());
                return true;
            case R.id.home_favourite:
                FavoritesActivity.start(this);
                return true;
            case R.id.home_message:
                DialogsActivity.start(this);
                return true;
            case R.id.home_notification:
                showNotificationDialog(findViewById(item.getItemId()));
                return true;
            case R.id.home_setting:
                return true;
            case R.id.home_logout:
                mPresenter.logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showNotificationDialog(View viewAttach) {
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        View view = this.getLayoutInflater().inflate(R.layout.notifications_dialog, null);
//        NestedScrollView scrollView = (NestedScrollView) view.findViewById(R.id.sv_notifications);
        view.setLayoutParams(new LinearLayout.LayoutParams(size.x - (int) (getResources().getDimension(R.dimen.activity_vertical_margin)),
                size.y - (2 * mToolbar.getHeight())));

        new EasyDialog(this)
                .setLayout(view)
                .setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
                .setLocationByAttachedView(viewAttach)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationAlphaShow(200, 0.0f, 1.0f)
                .setAnimationAlphaDismiss(200, 1.0f, 0.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(500, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(true)
                .setMarginLeftAndRight(24, 24)
                .setOutsideColor(ContextCompat.getColor(this, R.color.colorPrimary_50))
                .show();
    }

    @OnClick(R.id.fab_post)
    public void openPostActivity() {
        PostActivity.start(this);
    }

    @OnClick(R.id.iv_filter)
    public void showFilterDialog(View viewAttach) {
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        View view = this.getLayoutInflater().inflate(R.layout.filter_dialog, null);
//        NestedScrollView scrollView = (NestedScrollView) view.findViewById(R.id.sv_notifications);
//        FilterPagerAdapter adapter = new FilterPagerAdapter();
//        ViewPager pager = (ViewPager) view.findViewById(R.id.viewpager_filter);
//        pager.setAdapter(adapter);
        view.setLayoutParams(new LinearLayout.LayoutParams(size.x - (int) (getResources().getDimension(R.dimen.activity_vertical_margin)),
                size.y - (3 * mToolbar.getHeight())));
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.filter_tablayout);
        TabsViewPager viewPager = (TabsViewPager) view.findViewById(R.id.viewpager_filter);
        tabLayout.setupWithViewPager(viewPager);
        View PriceAndBedView = viewPager.getChildAt(0);
        RangeSeekBar rangeSeekbarPrice = (RangeSeekBar) view.findViewById(R.id.rangeseekbar_price);
        rangeSeekbarPrice.setNotifyWhileDragging(true);
        rangeSeekbarPrice.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {

//                Toast.makeText(getApplicationContext(), "Min Value- " + minValue + " & " + "Max Value- " + maxValue, Toast.LENGTH_LONG).show();
            }
        });
        RangeSeekBar rangeSeekbarBed = (RangeSeekBar) view.findViewById(R.id.rangeseekbar_bed);
        rangeSeekbarBed.setNotifyWhileDragging(true);
        rangeSeekbarBed.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {

//                Toast.makeText(getApplicationContext(), "Min Value- " + minValue + " & " + "Max Value- " + maxValue, Toast.LENGTH_LONG).show();
            }
        });


        new EasyDialog(this)
                .setLayout(view)
                .setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
                .setLocationByAttachedView(viewAttach)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationAlphaShow(200, 0.0f, 1.0f)
                .setAnimationAlphaDismiss(200, 1.0f, 0.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(500, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(true)
                .setMarginLeftAndRight(24, 24)
                .setOutsideColor(ContextCompat.getColor(this, R.color.colorPrimary_50))
                .show();
    }

//    class FilterPagerAdapter extends PagerAdapter {
//
//        public Object instantiateItem(View collection, int position) {
//
//            int resId = 0;
//            switch (position) {
//                case 0:
//                    resId = R.id.page_1;
//                    break;
//                case 1:
//                    resId = R.id.page_2;
//                    break;
//            }
//            return findViewById(resId);
//        }
//
//        @Override
//        public int getCount() {
//            return 2;
//        }
//
//        @Override
//        public boolean isViewFromObject(View arg0, Object arg1) {
//            return arg0 == ((View) arg1);
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    protected void subscribe() {
        super.subscribe();
        subscribeEvent(PostLikedEvent.class, this::onPostLikedEvent);
    }

    private void onPostLikedEvent(PostLikedEvent event) {
        try {
            if (event.isLiked())
                mPresenter.likePost( event.getPostId());
            else
                mPresenter.unLikePost(event.getPostId());
            Timber.e("onPostLikedEvent getPostId: %s ", event.getPostId() + "");
        } catch (Exception ex) {
            Timber.e("onPostLikedEvent %s ", ex);
        }
    }

    @Override
    public void displayPosts(ArrayList<PostObject> postObjects) {
        mAdapter.setData(postObjects);
    }

    @Override
    public void likePostById(long userId, long postId, PostObject.LikeStatus isLiked) {
        mAdapter.setLikedPostById(userId, postId, isLiked);
    }

    @Override
    public void openLogin() {
        AuthUtils.openLoginActivity(this);
    }
}