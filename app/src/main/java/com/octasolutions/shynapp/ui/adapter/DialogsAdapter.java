package com.octasolutions.shynapp.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.base.BaseSelectableListAdapter;
import com.octasolutions.shynapp.util.qb.QbDialogUtils;
import com.quickblox.chat.model.QBChatDialog;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DialogsAdapter extends BaseSelectableListAdapter<QBChatDialog> {

    private static final String EMPTY_STRING = "";

    public DialogsAdapter(Context context, List<QBChatDialog> dialogs) {
        super(context, dialogs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.li_dialog, parent, false);

            holder = new ViewHolder();
            holder.rootLayout = (ViewGroup) convertView.findViewById(R.id.rl_root);
            holder.mName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.mLastMessage = (TextView) convertView.findViewById(R.id.tv_messag_body);
            holder.mPhoto = (CircleImageView) convertView.findViewById(R.id.iv_person);
            holder.unreadIV = (ImageView) convertView.findViewById(R.id.iv_new_msg);
            holder.mDate = (TextView) convertView.findViewById(R.id.tv_date);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        QBChatDialog dialog = getItem(position);

        holder.mName.setText(QbDialogUtils.getDialogName(dialog));
        holder.mLastMessage.setText(prepareTextLastMessage(dialog));

        int unreadMessagesCount = getUnreadMsgCount(dialog);
        if (unreadMessagesCount == 0) {
            holder.unreadIV.setVisibility(View.GONE);
        } else {
            holder.unreadIV.setVisibility(View.VISIBLE);
        }

        holder.rootLayout.setBackgroundColor(isItemSelected(position) ? ContextCompat.getColor( convertView.getContext(), R.color.colorPrimary_50) :
                ContextCompat.getColor( convertView.getContext(),android.R.color.white));

        return convertView;
    }

    private int getUnreadMsgCount(QBChatDialog chatDialog){
        Integer unreadMessageCount = chatDialog.getUnreadMessageCount();
        if (unreadMessageCount == null) {
            return 0;
        } else {
            return unreadMessageCount;
        }
    }

    private boolean isLastMessageAttachment(QBChatDialog dialog) {
        String lastMessage = dialog.getLastMessage();
        Integer lastMessageSenderId = dialog.getLastMessageUserId();
        return TextUtils.isEmpty(lastMessage) && lastMessageSenderId != null;
    }

    private String prepareTextLastMessage(QBChatDialog chatDialog){
        if (isLastMessageAttachment(chatDialog)){
            return context.getString(R.string.chat_attachment);
        } else {
            return chatDialog.getLastMessage();
        }
    }

    private static class ViewHolder {
        ViewGroup rootLayout;
        CircleImageView mPhoto;
        TextView mName;
        TextView mLastMessage;
        ImageView unreadIV;
        TextView mDate;
    }
}
