package com.octasolutions.shynapp.ui.widget;

/**
 * @author HANZALA
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.octasolutions.shynapp.R;


/**
 * View pager used for a finite, low number of pages, where there is no need for
 * optimization.
 */
public class TabsViewPager extends ViewPager {

    /**
     * Initialize the view.
     *
     * @param context The application context.
     */
    public TabsViewPager(final Context context) {
        super(context);
    }

    /**
     * Initialize the view.
     *
     * @param context The application context.
     * @param attrs   The requested attributes.
     */
    public TabsViewPager(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // Make sure all are loaded at once
        final int childrenCount = getChildCount();
        setOffscreenPageLimit(childrenCount - 1);

        // Attach the adapter
        setAdapter(new PagerAdapter() {

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getResources().getString(R.string.tab_price_bed);
                    case 1:
                        return getResources().getString(R.string.tab_property_type);
                }
                return super.getPageTitle(position);
            }

            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {
                return container.getChildAt(position);
            }

            @Override
            public boolean isViewFromObject(final View arg0, final Object arg1) {
                return arg0 == arg1;

            }

            @Override
            public int getCount() {
                return childrenCount;
            }

            @Override
            public void destroyItem(final View container, final int position, final Object object) {
            }
        });
    }

}