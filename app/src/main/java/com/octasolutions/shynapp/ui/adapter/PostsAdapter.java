package com.octasolutions.shynapp.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.ui.adapter.viewHolder.PostItemHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HANZALA
 */
public class PostsAdapter extends RecyclerView.Adapter<PostItemHolder> {
    private List<PostObject> mList;

    public PostsAdapter() {
        mList = new ArrayList<>();
    }

    @Override
    public PostItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_post_property, parent, false);
        return new PostItemHolder(view);
    }

    @Override
    public void onBindViewHolder(PostItemHolder holder, int position) {
        holder.bindData(mList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setData(@NonNull List<PostObject> dataList) {
        mList = dataList;
        notifyDataSetChanged();
    }

    public void setLikedPostById(long userId, long postId, PostObject.LikeStatus isLiked) {
        for (PostObject p : mList) {
            if (p.getId() == postId /*&& p.getUserId() == userId*/) {
                p.setIsLiked(isLiked);
                //TODO: enable this when data will be accurate
//                break;
            }
        }
        notifyDataSetChanged();
    }
}