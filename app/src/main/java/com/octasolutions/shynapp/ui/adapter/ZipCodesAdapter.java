package com.octasolutions.shynapp.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.ZipCodeObject;
import com.octasolutions.shynapp.ui.adapter.viewHolder.ZipCodeItemHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HANZALA
 */

public class ZipCodesAdapter extends RecyclerView.Adapter<ZipCodeItemHolder> {
    private List<ZipCodeObject> mList;
    private ZipCodeType mType;

    public ZipCodesAdapter() {
        mList = new ArrayList<>();
    }

    @Override
    public ZipCodeItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_zip_code, parent, false);
        return new ZipCodeItemHolder(view);
    }

    @Override
    public void onBindViewHolder(ZipCodeItemHolder holder, int position) {
        holder.bindData(mList.get(position), position, mType);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setData(@NonNull List<ZipCodeObject> dataList, ZipCodeType type) {
        mList = dataList;
        mType = type;
        notifyDataSetChanged();
    }

    public static enum ZipCodeType {
        AVAILABLE,
        SELECTED
    }
}