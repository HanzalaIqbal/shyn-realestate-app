package com.octasolutions.shynapp.ui.presenter;

import android.text.TextUtils;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.SetProfileResponse;
import com.octasolutions.shynapp.ui.mvpview.SetProfileMvpView;
import com.octasolutions.shynapp.util.RxUtil;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import rx.subscriptions.CompositeSubscription;

/**
 * @author HANZALA
 */
public class SetProfilePresenter extends PhotoPresenter<SetProfileMvpView> {
    private final DataManager dataManager;
    private CompositeSubscription subscription;

    @Inject
    public SetProfilePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void onPhotoClick() {
        if (isViewAttached())
            getMvpView().showPhotoSourceChooserDialog();
    }

    public void setProfile(MultipartBody.Part filePart, String displayName, String companyName, String webAddress, String homeAddress) {
        checkViewAttached();

        if (TextUtils.isEmpty(displayName) || TextUtils.isEmpty(companyName)|| TextUtils.isEmpty(webAddress)|| TextUtils.isEmpty(homeAddress)) {
            getMvpView().displayToast(R.string.empty_fields);
            return;
        }

        subscription.add(
                dataManager.setProfile(filePart, displayName,companyName,webAddress,homeAddress)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleSetProfileResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handleSetProfileResponse(SetProfileResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                dataManager.userId().set((long) response.data.getUserId());
                getMvpView().onSetProfileSuccess();
                getMvpView().displayToast(response.msg);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }
}