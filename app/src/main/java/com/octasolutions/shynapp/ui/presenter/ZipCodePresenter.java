package com.octasolutions.shynapp.ui.presenter;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.remote.response.AllStatesResponse;
import com.octasolutions.shynapp.data.remote.response.GetCitiesResponse;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.PromotionsResponse;
import com.octasolutions.shynapp.data.remote.response.PurchaseZipCodesResponse;
import com.octasolutions.shynapp.data.remote.response.ZipCodesResponse;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.ZipCodeMvpView;
import com.octasolutions.shynapp.util.RxUtil;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

/**
 * @author HANZALA
 */

public class ZipCodePresenter extends BasePresenter<ZipCodeMvpView> {

    private final DataManager dataManager;
    private CompositeSubscription subscription;

    @Inject
    public ZipCodePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void getAllStates() {
        checkViewAttached();
        subscription.add(
                dataManager.getAllStates()
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleAllStatesResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );

    }

    private void handleAllStatesResponse(AllStatesResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onAllStatesResult(response.states);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void getCities(String stateId) {
        checkViewAttached();
        subscription.add(
                dataManager.getCities(stateId)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleCitiesResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handleCitiesResponse(GetCitiesResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onAllCitiesResult(response.cities);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void getZipCodes(long cityId) {
        checkViewAttached();
        subscription.add(
                dataManager.getZipCodes(cityId)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleZipCodesResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handleZipCodesResponse(ZipCodesResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onZipCodesResult(response.zipCodes);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void getPromotions() {
        checkViewAttached();
        subscription.add(
                dataManager.getPromotions()
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handlePromotionsResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handlePromotionsResponse(PromotionsResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onPromotionResult(response.promotion);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void purchaseZipcode(String zipCodeIDs, String paymentDetail, int yearly, String dateTime) {
        checkViewAttached();
        subscription.add(
                dataManager.purchaseZipcode(zipCodeIDs, paymentDetail, yearly, dateTime)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleZipcodeResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handleZipcodeResponse(PurchaseZipCodesResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onPurchaseZipCodesResult(response.msg);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }
}
