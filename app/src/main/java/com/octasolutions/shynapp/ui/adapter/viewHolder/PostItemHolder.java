package com.octasolutions.shynapp.ui.adapter.viewHolder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.event.PostLikedEvent;
import com.octasolutions.shynapp.ui.view.PostDetailActivity;
import com.octasolutions.shynapp.ui.view.ProfileActivity;
import com.octasolutions.shynapp.util.CommonUtils;
import com.octasolutions.shynapp.util.RxUtil;
import com.octasolutions.shynapp.util.UrlUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * @author HANZALA
 */
public class PostItemHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_post_photo)
    ImageView mPhoto;
    @BindView(R.id.tv_ask_price)
    TextView mAskPrice;
    @BindView(R.id.tv_beds)
    TextView mBeds;
    @BindView(R.id.tv_baths)
    TextView mBaths;
    @BindView(R.id.tv_est_avg)
    TextView mEstAvg;
    @BindView(R.id.tv_est_rep)
    TextView mEstRep;
    @BindView(R.id.tv_sq_feet)
    TextView mSqFeet;
    @BindView(R.id.tv_location)
    TextView mLocation;
    @BindView(R.id.iv_fav_icon)
    ImageView mFavorite;

    private PostObject mPostObject;
    private Context context;

    public PostItemHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindData(PostObject order, int position) {
        mPostObject = order;
        context = itemView.getContext();
        Picasso.with(context)
                .load(UrlUtils.getPictureUrl(mPostObject.getImg1()))
                .centerCrop()
                .fit()
                .into(mPhoto);
        if (mPostObject.getIsLiked() == PostObject.LikeStatus.LIKED)
            mFavorite.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary));
        else
            mFavorite.setColorFilter(ContextCompat.getColor(context, R.color.white));
        mAskPrice.setText(String.format(context.getString(R.string.cost), mPostObject.getAskPrice()));
        mBeds.setText(String.format(context.getString(R.string.s_beds), mPostObject.getBeds()));
        mBaths.setText(String.format(context.getString(R.string.s_baths), mPostObject.getBaths()));
        mEstAvg.setText(String.format(context.getString(R.string.cost), mPostObject.getEstARV()));
        mEstRep.setText(String.format(context.getString(R.string.cost), mPostObject.getEstRepCost()));
        mSqFeet.setText(String.format(context.getString(R.string.s_sqft), mPostObject.getSqFeet()));
        mLocation.setText(String.format(context.getString(R.string.post_address),
                mPostObject.getHouseNo(), mPostObject.getStreetNo(), mPostObject.getCityName()));
    }

    @OnClick(R.id.iv_fav_icon)
    public void likePost(View view) {
        if (mPostObject.getIsLiked() == PostObject.LikeStatus.UN_LIKED)
            RxUtil.post(view.getContext(), new PostLikedEvent(mPostObject.getId(), true));
        else
            RxUtil.post(view.getContext(), new PostLikedEvent(mPostObject.getId(), false));
    }

    @OnClick(R.id.iv_share_post)
    public void sharePost(View view) {
        CommonUtils.sharePost(view.getContext(), mPostObject);
    }

    @OnClick(R.id.iv_open_chat)
    public void openChat(View view) {
//        ChatActivity.start(view.getContext());
    }

    @OnClick(R.id.iv_open_profile)
    public void openProfile(View view) {
        ProfileActivity.start(view.getContext(), mPostObject.getUserId());
    }

    @Optional
    @OnClick({R.id.rl_address, R.id.ll_details_post, R.id.iv_post_photo})
    public void openDetailActivity(View view) {
        PostDetailActivity.start(view.getContext(),mPostObject.getId());
    }
}