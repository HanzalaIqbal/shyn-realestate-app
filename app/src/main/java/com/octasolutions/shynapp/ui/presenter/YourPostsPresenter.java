package com.octasolutions.shynapp.ui.presenter;

import android.support.v4.util.Pair;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.data.remote.request.LikePostRequest;
import com.octasolutions.shynapp.data.remote.response.GetPostsResponse;
import com.octasolutions.shynapp.data.remote.response.LikeResponse;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.UnLikeResponse;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.YourPostsMvpView;
import com.octasolutions.shynapp.util.RxUtil;

import javax.inject.Inject;

import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * @author HANZALA
 */

public class YourPostsPresenter extends BasePresenter<YourPostsMvpView> {
    private final DataManager dataManager;
    private CompositeSubscription subscription;

    @Inject
    public YourPostsPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void getMyPosts(long mUserId) {
        checkViewAttached();
        subscription.add(
                dataManager.getPosts(mUserId)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handlePostsResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );

    }

    private void handlePostsResponse(GetPostsResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().displayYourPosts(response.data);
            } else {
                getMvpView().displayYourPosts(response.data);
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void likePost(long postId) {
        checkViewAttached();

        final LikePostRequest request = new LikePostRequest(dataManager.userId().get(), postId, true);
        subscription.add(
                Observable.just(request)
                        .withLatestFrom(dataManager.likePost(request), Pair::new)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleLikeResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handleLikeResponse(Pair<LikePostRequest, LikeResponse> pair) {
        final LikePostRequest request = pair.first;
        final LikeResponse response = pair.second;
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().likePostById(request.getUserId(), request.getPostId(), PostObject.LikeStatus.LIKED);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void unLikePost(long postId) {
        checkViewAttached();

        final LikePostRequest request = new LikePostRequest(dataManager.userId().get(), postId, false);
        subscription.add(
                Observable.just(request)
                        .withLatestFrom(dataManager.unLikePost(request), Pair::new)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleUnLikeResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handleUnLikeResponse(Pair<LikePostRequest, UnLikeResponse> pair) {
        final LikePostRequest request = pair.first;
        final UnLikeResponse response = pair.second;
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().likePostById(request.getUserId(), request.getPostId(), PostObject.LikeStatus.UN_LIKED);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }
}
