package com.octasolutions.shynapp.ui.mvpview;

import com.octasolutions.shynapp.ui.base.MvpView;

/**
 * @author HANZALA
 */

public interface ChoseUserMvpView extends MvpView {
    void onSetUserType();
}
