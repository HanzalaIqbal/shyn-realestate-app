package com.octasolutions.shynapp.ui.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.octasolutions.shynapp.Henson;
import com.octasolutions.shynapp.ShynApplication;
import com.octasolutions.shynapp.event.OnSingleChoiceOptionSelectedEvent;
import com.octasolutions.shynapp.util.RxEventBus;

import javax.inject.Inject;

/**
 * @author Mike
 */
public class SingleChoiceDialog extends DialogFragment {

    @Inject
    RxEventBus mEventBus;

    @InjectExtra
    @ArrayRes
    int mOptions;

    public static void show(@NonNull Context context,
                            @NonNull FragmentManager fm,
                            @ArrayRes int options) {
        final SingleChoiceDialog dialog = new SingleChoiceDialog();
        final Bundle args = Henson.with(context)
                .gotoSingleChoiceDialog()
                .mOptions(options)
                .build()
                .getExtras();
        dialog.setArguments(args);
        dialog.show(fm, SingleChoiceDialog.class.getName());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShynApplication.get(getActivity()).getComponent().inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Dart.inject(this, getArguments());
        builder.setItems(mOptions, (dialogInterface, i) -> mEventBus.post(new OnSingleChoiceOptionSelectedEvent(i)));
        return builder.create();
    }

}