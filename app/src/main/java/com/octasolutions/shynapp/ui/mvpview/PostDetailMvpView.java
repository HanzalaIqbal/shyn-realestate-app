package com.octasolutions.shynapp.ui.mvpview;

import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.ui.base.MvpView;

import java.util.ArrayList;

/**
 * @author HANZALA
 */

public interface PostDetailMvpView extends MvpView {
    void displayPostDetail(PostObject data);

    void displayImages(ArrayList<String> img);
}
