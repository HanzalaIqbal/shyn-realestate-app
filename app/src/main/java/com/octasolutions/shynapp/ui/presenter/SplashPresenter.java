package com.octasolutions.shynapp.ui.presenter;

import android.os.Bundle;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.SplashMvpView;
import com.octasolutions.shynapp.util.chat.ChatHelper;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class SplashPresenter extends BasePresenter<SplashMvpView> {

    private final DataManager mDataManager;
    private boolean loginSuccessful;

    @Inject
    public SplashPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    public void tryLogin() {
        checkViewAttached();
        if (mDataManager.userId().get() > 0) loginSuccessful = true;
        else loginSuccessful = false;
        getMvpView().login();
    }

    public boolean isLoginSuccessful() {
        return loginSuccessful;
    }

    public void restoreChatSession() {
        if (ChatHelper.getInstance().isLogged()) {
//            DialogsActivity.start(this);
//            finish();
        } else {
            getUserByID(Integer.parseInt(mDataManager.userChatID().get()), mDataManager.userPassword().get());
        }
    }

    private void getUserByID(int chatID, String password) {
        List<Integer> ids = new ArrayList<>();
        ids.add(chatID);

        QBUsers.getUsersByIDs(ids, null).performAsync(new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> result, Bundle params) {
                if (result.isEmpty()) {
                    Timber.w("No user found");
                    return;
                }
                QBUser qbUser = result.get(0);
                qbUser.setPassword(password);
                loginQB(qbUser);
            }

            @Override
            public void onError(QBResponseException e) {
                Timber.w(e, "No user found");
            }
        });

    }

    private void loginQB(final QBUser user) {
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Timber.w("login Onsuccess ");
            }

            @Override
            public void onError(QBResponseException e) {
                Timber.e(e, "login error %s", e.getErrors().toArray().toString());
            }
        });
    }
}
