package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.event.PostLikedEvent;
import com.octasolutions.shynapp.ui.adapter.PostsAdapter;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.FavoriteMvpView;
import com.octasolutions.shynapp.ui.presenter.FavoritesPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * @author HANZALA
 */
public class FavoritesActivity extends BaseActivity implements FavoriteMvpView {

    @Inject
    FavoritesPresenter mPresenter;

    @BindView(R.id.rv_fav_posts)
    RecyclerView mPostsList;

    PostsAdapter mAdapter;

    public static void start(Context context) {
        context.startActivity(new Intent(context, FavoritesActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        initToolbar();

        mPresenter.attachView(this);

        mAdapter = new PostsAdapter();
        mPostsList.setLayoutManager(new LinearLayoutManager(this));
        mPostsList.setAdapter(mAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getFavoriteProperties();
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
        setNavigationIcon(R.drawable.ic_home);
        setActionBarTitle(R.string.ab_favourites);
    }

    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_favourites;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    protected void subscribe() {
        super.subscribe();
        subscribeEvent(PostLikedEvent.class, this::onPostLikedEvent);
    }

    private void onPostLikedEvent(PostLikedEvent event) {
        try {
            if (event.isLiked())
                mPresenter.likePost(event.getPostId());
            else
                mPresenter.unLikePost(event.getPostId());
            Timber.e("onPostLikedEvent getPostId: %s ", event.getPostId() + "");
        } catch (Exception ex) {
            Timber.e("onPostLikedEvent %s ", ex);
        }
    }

    @Override
    public void displayFavoritePosts(ArrayList<PostObject> data) {
        mAdapter.setData(data);
    }

    @Override
    public void likePostById(long userId, long postId, PostObject.LikeStatus liked) {
        mAdapter.setLikedPostById(userId, postId, liked);
        mPresenter.getFavoriteProperties();
    }
}