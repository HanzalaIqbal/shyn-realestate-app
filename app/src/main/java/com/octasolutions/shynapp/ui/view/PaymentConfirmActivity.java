package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.octasolutions.shynapp.Henson;
import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author HANZALA
 */
public class PaymentConfirmActivity extends BaseActivity {

    @BindView(R.id.tv_payment)
    TextView mPayment;
    @BindView(R.id.tv_payment_id)
    TextView mCode;

    @InjectExtra
    String mPaymentID;
    @InjectExtra
    double mTotalPayment = -1;

    public static void start(Context context, double totalPayment, String paymentID) {
        final Intent intent = Henson.with(context)
                .gotoPaymentConfirmActivity()
                .mPaymentID(paymentID)
                .mTotalPayment(totalPayment)
                .build();
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Dart.inject(this);
        initToolbar();

        initView();
        initToolbar();

    }

    private void initView() {
        mPayment.setText(String.format("$%s", mTotalPayment + ""));
        mCode.setText(mPaymentID);
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
        setNavigationIcon(R.drawable.ic_home);
        setActionBarTitle(R.string.ab_successful_payment);
    }


    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_payment_confirm;
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.inbox_menu, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        HomeActivity.start(this);
    }
}