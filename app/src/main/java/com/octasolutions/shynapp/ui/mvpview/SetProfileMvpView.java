package com.octasolutions.shynapp.ui.mvpview;

/**
 * @author HANZALA
 */

public interface SetProfileMvpView extends PhotoView {
    void showPhotoSourceChooserDialog();

    void onSetProfileSuccess();
}
