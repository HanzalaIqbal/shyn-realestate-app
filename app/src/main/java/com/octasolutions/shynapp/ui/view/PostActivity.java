package com.octasolutions.shynapp.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.PurchasedCityObject;
import com.octasolutions.shynapp.data.model.StateObject;
import com.octasolutions.shynapp.event.AddPhotoClickedEvent;
import com.octasolutions.shynapp.ui.adapter.ImageViewsAdapter;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.NewPostMvpView;
import com.octasolutions.shynapp.ui.presenter.NewPostPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.myinnos.awesomeimagepicker.activities.AlbumSelectActivity;
import in.myinnos.awesomeimagepicker.helpers.ConstantsCustomGallery;
import in.myinnos.awesomeimagepicker.models.Image;
import timber.log.Timber;

/**
 * @author HANZALA
 */
public class PostActivity extends BaseActivity implements NewPostMvpView {

    @Inject
    NewPostPresenter mPresenter;

    @BindView(R.id.rv_images)
    RecyclerView mImagesList;
    @BindView(R.id.et_house_no)
    EditText mHouseNo;
    @BindView(R.id.et_street)
    EditText mStreet;
    @BindView(R.id.spinner_state)
    Spinner mStates;
    @BindView(R.id.spinner_city)
    Spinner mCities;
    @BindView(R.id.et_asking_price)
    EditText mAskingPrice;
    @BindView(R.id.et_sq_feet)
    EditText mSqFeet;
    @BindView(R.id.et_repair_cost)
    EditText mRepairCost;
    @BindView(R.id.et_avr)
    EditText mAvr;
    @BindView(R.id.tab_fixer)
    TabLayout mFixer;
    @BindView(R.id.et_notes)
    EditText mNotes;
    @BindView(R.id.tv_beds)
    TextView tvBeds;
    @BindView(R.id.sb_beds)
    SeekBar mBeds;
    @BindView(R.id.tv_baths)
    TextView tvBaths;
    @BindView(R.id.sb_baths)
    SeekBar mBaths;

    ImageViewsAdapter mAdapter;
    private ArrayList<Image> mImages = new ArrayList<>();
    private int MAX_SIZE = 5;
    private ArrayList<PurchasedCityObject> mPurchasedCities;
    private PurchasedCityObject mCity;

    private SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (seekBar.getId() == mBeds.getId()) {
                tvBeds.setText(String.format("%d Beds", progress));
            } else if (seekBar.getId() == mBaths.getId()) {
                tvBaths.setText(String.format("%d Baths", progress));
            }

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    public static void start(Context context) {
        context.startActivity(new Intent(context, PostActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initToolbar();
        activityComponent().inject(this);
        mPresenter.attachView(this);

        mAdapter = new ImageViewsAdapter();
        mImagesList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImagesList.setAdapter(mAdapter);
        mAdapter.setData(new ArrayList<>());

        mPresenter.getAllStates();
        mPresenter.getPurchasedCities();
        initView();
    }

    private void initView() {
        mBeds.setOnSeekBarChangeListener(seekBarListener);
        mBaths.setOnSeekBarChangeListener(seekBarListener);
        mBaths.getProgress();
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
        setNavigationIcon(R.drawable.ic_home);
        setActionBarTitle(R.string.ab_post);
    }


    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_post;
    }

    protected void subscribe() {
        super.subscribe();
        subscribeEvent(AddPhotoClickedEvent.class, this::onAddPhotoClickedEvent);
    }

    private void onAddPhotoClickedEvent(AddPhotoClickedEvent event) {
        Intent intent = new Intent(this, AlbumSelectActivity.class);
        intent.putExtra(ConstantsCustomGallery.INTENT_EXTRA_LIMIT, MAX_SIZE - mImages.size()); // set limit for image selection
        startActivityForResult(intent, ConstantsCustomGallery.REQUEST_CODE);
    }

    @Override
    public void onAllStatesResult(ArrayList<StateObject> states) {
        states.add(0, new StateObject(getResources().getString(R.string.select_state)));
        if (states.size() == 1)
            return;

        Timber.w("Size %s", states.size());
        ArrayAdapter<StateObject> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, states); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStates.setAdapter(spinnerArrayAdapter);
        mStates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position == 0)
//                    return;
//                mPresenter.getCities(states.get(position).getStateId());
//                mState = states.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onPurchasedCitiesResult(ArrayList<PurchasedCityObject> cities) {
//        mPurchasedCities = cities;
        if (cities.size() >= 0)
            cities.add(0, new PurchasedCityObject(getResources().getString(R.string.select_city)));
        if (cities.size() == 0)
            cities.add(new PurchasedCityObject("Purchase a zip code first"));

        ArrayAdapter<PurchasedCityObject> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cities); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCities.setAdapter(spinnerArrayAdapter);
        mCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    return;
                mCity = cities.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onPostSuccess() {
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ConstantsCustomGallery.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            mImages.addAll(data.getParcelableArrayListExtra(ConstantsCustomGallery.INTENT_EXTRA_IMAGES));
            mAdapter.setData(mImages);
//            for (int i = 0; i < images.size(); i++) {
//                Uri uri = Uri.fromFile(new File(images.get(i).path));
//
//                File file = new File(uri.getPath());
////                Glide.with(this).load(uri)
////                        .placeholder(R.color.colorAccent)
////                        .override(400, 400)
////                        .crossFade()
////                        .centerCrop()
////                        .into(imageView);
//
//                Timber.w(String.valueOf(i + 1) + ". " + file.getAbsolutePath());
//                Timber.w(String.valueOf(i + 1) + ". " + String.valueOf(uri));
//            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @OnClick(R.id.tv_btn_post)
    void openChatActivity(View view) {
        mPresenter.createPost(mHouseNo.getText().toString(), mStreet.getText().toString(), mCity != null ? mCity.getCityId() : 0, mAskingPrice.getText().toString(),
                mSqFeet.getText().toString(), mRepairCost.getText().toString(), mAvr.getText().toString(), mFixer.getSelectedTabPosition(),
                mNotes.getText().toString(), mBeds.getProgress(), mBaths.getProgress(), mImages);
    }
}