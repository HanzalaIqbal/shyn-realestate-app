package com.octasolutions.shynapp.ui.mvpview;

import com.octasolutions.shynapp.ui.base.MvpView;

/**
 * @author HANZALA
 */

public interface SignUpMvpView extends MvpView {
    void signupSuccess(String msg);
}
