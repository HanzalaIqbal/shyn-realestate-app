package com.octasolutions.shynapp.ui.presenter;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.remote.response.AllStatesResponse;
import com.octasolutions.shynapp.data.remote.response.CreatePostResponse;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.PurchasedCitiesResponse;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.NewPostMvpView;
import com.octasolutions.shynapp.util.RxUtil;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import in.myinnos.awesomeimagepicker.models.Image;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.subscriptions.CompositeSubscription;

/**
 * @author HANZALA
 */

public class NewPostPresenter extends BasePresenter<NewPostMvpView> {
    private final DataManager dataManager;
    private CompositeSubscription subscription;

    @Inject
    public NewPostPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void getAllStates() {
        checkViewAttached();
        subscription.add(
                dataManager.getAllStates()
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleAllStatesResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );

    }

    private void handleAllStatesResponse(AllStatesResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onAllStatesResult(response.states);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void getPurchasedCities() {
        checkViewAttached();
        subscription.add(
                dataManager.getPurchasedCities()
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleCitiesResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handleCitiesResponse(PurchasedCitiesResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onPurchasedCitiesResult(response.cities);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void createPost(String houseNo, String street, long cityId, String askingPrice,
                           String sqFeet, String repairCost, String avr, int isFixer, String notes,
                           int beds, int baths, ArrayList<Image> images) {
        checkViewAttached();

        if (images.isEmpty()) {
            getMvpView().displayToast("Select any photo");
            return;
        }
        if (houseNo.isEmpty() || street.isEmpty() || cityId == 0 || askingPrice.isEmpty() || sqFeet.isEmpty() ||
                repairCost.isEmpty() || avr.isEmpty() || notes.isEmpty()) {
            getMvpView().displayToast("All fields are required");
            return;
        }


        File img1;
        MultipartBody.Part image1;
        File img2;
        MultipartBody.Part image2 = null;
        File img3;
        MultipartBody.Part image3 = null;
        File img4;
        MultipartBody.Part image4 = null;
        File img5;
        MultipartBody.Part image5 = null;

        img1 = new File(images.get(0).path);
        image1 = MultipartBody.Part.createFormData("img1", img1.getName(), RequestBody.create(MediaType.parse("image/*"), img1));
        if (images.size() > 1) {
            img2 = new File(images.get(1).path);
            image2 = MultipartBody.Part.createFormData("img2", img2.getName(), RequestBody.create(MediaType.parse("image/*"), img2));
        }
        if (images.size() > 2) {
            img3 = new File(images.get(2).path);
            image3 = MultipartBody.Part.createFormData("img3", img3.getName(), RequestBody.create(MediaType.parse("image/*"), img3));
        }
        if (images.size() > 3) {
            img4 = new File(images.get(3).path);
            image4 = MultipartBody.Part.createFormData("img4", img4.getName(), RequestBody.create(MediaType.parse("image/*"), img4));
        }
        if (images.size() > 4) {
            img5 = new File(images.get(4).path);
            image5 = MultipartBody.Part.createFormData("img5", img5.getName(), RequestBody.create(MediaType.parse("image/*"), img5));
        }


        subscription.add(
                dataManager.createPost(houseNo, street, cityId, askingPrice, sqFeet, repairCost, avr,
                        isFixer, notes, beds, baths, image1, image2, image3, image4, image5)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleCreatePost,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );
    }

    private void handleCreatePost(CreatePostResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onPostSuccess();
                getMvpView().displayToast("Property posted successfully");
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }
}
