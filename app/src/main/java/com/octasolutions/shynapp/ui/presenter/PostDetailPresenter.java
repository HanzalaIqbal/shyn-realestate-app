package com.octasolutions.shynapp.ui.presenter;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.PropertyDetailResponse;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.PostDetailMvpView;
import com.octasolutions.shynapp.util.RxUtil;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

/**
 * @author HANZALA
 */
public class PostDetailPresenter extends BasePresenter<PostDetailMvpView> {

    private final DataManager dataManager;
    private CompositeSubscription subscription;

    @Inject
    public PostDetailPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void getPostDetails(long mPostId) {
        checkViewAttached();
        subscription.add(
                dataManager.getPostDetail(mPostId)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handlePropertyDetailResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );

    }

    private void handlePropertyDetailResponse(PropertyDetailResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().displayPostDetail(response.data);
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }

    public void showImages(ArrayList<String> img) {
        getMvpView().displayImages(img);
    }
}