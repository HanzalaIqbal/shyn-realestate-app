package com.octasolutions.shynapp.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.event.OnSingleChoiceOptionSelectedEvent;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.SetProfileMvpView;
import com.octasolutions.shynapp.ui.presenter.SetProfilePresenter;
import com.octasolutions.shynapp.util.PermissionUtil;
import com.octasolutions.shynapp.util.PhotoUtil;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * @author HANZALA
 */
public class SetProfileActivity extends BaseActivity implements SetProfileMvpView, PermissionUtil.RationaleCallback {

    @Inject
    SetProfilePresenter mPresenter;

    private Uri mPhotoImageUri;

    @BindView(R.id.iv_photo)
    ImageView mPhoto;
    @BindView(R.id.et_display_name)
    EditText mDisplayName;
    @BindView(R.id.et_company_name)
    EditText mCompanyName;
    @BindView(R.id.et_web_address)
    EditText mWebAddress;
    @BindView(R.id.et_address)
    EditText mHomeAddress;

    private int requestCode;

    public static void start(Context context) {
        final Intent startIntent = new Intent(context, SetProfileActivity.class);
        //TODO: change this after this activity completed
//        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initToolbar();
        activityComponent().inject(this);
        mPresenter.attachView(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_set_profile;
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            findViewById(R.id.action_logo).setVisibility(View.VISIBLE);
        }
//        setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
    }


    @OnClick(R.id.iv_photo)
    public void openPhotoDialog(View view) {
//        ZipCodePurchaseActivity.start(this);
        mPresenter.onPhotoClick();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_done:
                setProfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setProfile() {
        if (mPhotoImageUri == null || mPhotoImageUri.getPath().isEmpty()) {
            displayToast(R.string.empty_fields);
            return;
        }

        File file;
        if (requestCode == PhotoUtil.REQUEST_CODE_PICK_PHOTO)
            file = new File(PhotoUtil.getRealPathFromURI(this, mPhotoImageUri));
        else
            file = new File(mPhotoImageUri.getPath());
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("txtPhoto", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        mPresenter.setProfile(filePart, mDisplayName.getText().toString(), mCompanyName.getText().toString(),
                mWebAddress.getText().toString(), mHomeAddress.getText().toString());
    }

    @Override
    public void showPhotoSourceChooserDialog() {
        SingleChoiceDialog.show(this, getSupportFragmentManager(), mPresenter.getPhotoSourceOptions());
    }

    @Override
    public void onSetProfileSuccess() {
        ZipCodePurchaseActivity.start(this);
    }

    @Override
    protected void subscribe() {
        super.subscribe();
        subscribeEvent(OnSingleChoiceOptionSelectedEvent.class, this::onSelectOption);
    }

    public void onSelectOption(OnSingleChoiceOptionSelectedEvent event) {
        mPresenter.onPhotoSourcePicked(event.getOption(), true);
    }

    @Override
    public void openGallery() {
        PhotoUtil.openGallery(this);
    }

    @Override
    public void openCamera() {
        mPhotoImageUri = Uri.fromFile(PhotoUtil.openCamera(this));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (PhotoUtil.REQUEST_CODE_TAKE_PHOTO == requestCode) {
            this.requestCode = requestCode;
            if (Activity.RESULT_OK == resultCode && mPhotoImageUri != null) {
                loadImageUri();
            }
        } else if (requestCode == PhotoUtil.REQUEST_CODE_PICK_PHOTO) {
            this.requestCode = requestCode;
            if (Activity.RESULT_OK == resultCode && data != null) {
                mPhotoImageUri = data.getData();
                loadImageUri();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loadImageUri() {
        Picasso.with(this)
                .load(mPhotoImageUri)
                .into(mTarget);
                /*.into(mProfilePhoto);*/
    }

    private Target mTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            mPresenter.onPhotoPicked(bitmap, mPhotoImageUri);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            // TODO: show error
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            // TODO: show loading?
        }
    };

    @Override
    public void displayPhoto(Bitmap bitmap) {
        mPhoto.setVisibility(View.VISIBLE);
        mPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mPhoto.setImageBitmap(bitmap);
    }

    @Override
    public void setPlaceHolderImage() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean isPermissionGranted(@NonNull String permission) {
        return PermissionUtil.isPermissionGranted(this, permission);
    }

    @Override
    public void showPermissionRationaleDialog(@StringRes int rationale) {
        PermissionUtil.showRationaleDialog(this, rationale, this);
    }

    @Override
    public void rationaleButtonClick(boolean isPositive) {
        if (isPositive) {
            mPresenter.onPhotoSourcePicked();
        }
    }
}