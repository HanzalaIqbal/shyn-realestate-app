package com.octasolutions.shynapp.ui.mvpview;

import android.graphics.Bitmap;

/**
 * @author HANZALA
 */
public interface PhotoView extends PermissionsView {
    void openGallery();
    void openCamera();
    void displayPhoto(Bitmap bitmap);
    void setPlaceHolderImage();
}
