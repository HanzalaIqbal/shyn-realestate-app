package com.octasolutions.shynapp.ui.presenter;

import android.support.annotation.NonNull;

import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.PermissionsView;
import com.octasolutions.shynapp.util.PermissionUtil;


/**
 * @author Mike
 */
public abstract class PermissionsPresenter<T extends PermissionsView> extends BasePresenter<T> {

    protected PermissionsView mView;

    public PermissionsPresenter() {
    }

    @Override
    public void attachView(T mvpView) {
        super.attachView(mvpView);
        mView = mvpView;
    }

    @Override
    public void detachView() {
        super.detachView();
        mView = null;
    }

    public boolean checkAndRequestPermission(boolean showAlertDialog, final PermissionUtil.Permission... permissions) {
        for (PermissionUtil.Permission permission : permissions) {
            if (!checkAndRequestPermission(permission, showAlertDialog)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkAndRequestPermission(PermissionUtil.Permission permission, boolean showAlertDialog) {
        if (!isPermissionGranted(permission.getPermission())) {
            final boolean shouldShowRationale = shouldShowRequestPermissionRationale(permission.getPermission());
            if (showAlertDialog && shouldShowRationale) {
                showPermissionRationaleDialog(permission);
            } else {
                requestPermission(permission);
            }
            return false;
        }

        return true;
    }

    public abstract void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);

    public boolean isPermissionGranted(@NonNull String permission) {
        return mView.isPermissionGranted(permission);
    }

    public boolean shouldShowRequestPermissionRationale(@NonNull String permission) {
        return mView.shouldShowRequestPermissionRationale(permission);
    }

    public void requestPermission(PermissionUtil.Permission permission) {
        mView.requestPermissions(new String[]{permission.getPermission()}, permission.getRequestCode());
    }

    public void showPermissionRationaleDialog(PermissionUtil.Permission permission) {
        mView.showPermissionRationaleDialog(permission.getRationale());
    }
}
