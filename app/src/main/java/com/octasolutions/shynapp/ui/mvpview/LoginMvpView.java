package com.octasolutions.shynapp.ui.mvpview;

import android.support.annotation.StringRes;

import com.octasolutions.shynapp.ui.base.MvpView;

public interface LoginMvpView extends MvpView {

    void loginSuccess();

    void loginFailed();

    void loginFailed(String description);

    void loginFailed(@StringRes int reason);


    void registerFacebook(String facebookAccessToken, String email);
}
