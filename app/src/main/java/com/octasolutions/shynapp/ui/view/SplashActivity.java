package com.octasolutions.shynapp.ui.view;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Window;
import android.view.WindowManager;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.SplashMvpView;
import com.octasolutions.shynapp.ui.presenter.SplashPresenter;
import com.octasolutions.shynapp.util.AuthUtils;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity implements SplashMvpView {
    private static final int SPLASH_DURATION = 2000;

    @Inject
    SplashPresenter mSplashPresenter;
    private CountDownTimer mTimer;
    private boolean mTimerFinished;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        activityComponent().inject(this);

        mSplashPresenter.attachView(this);
        mSplashPresenter.tryLogin();
    }

    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_splash;
    }

    @Override
    protected void onResume() {
        super.onResume();
        cancelTimer();
        mTimer = getSplashTimer();
        mTimer.start();
    }

    @Override
    protected void onPause() {
        cancelTimer();
        super.onPause();
    }

    private void cancelTimer() {
        mTimerFinished = false;
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSplashPresenter.detachView();
    }

    private CountDownTimer getSplashTimer() {
        return new CountDownTimer(SPLASH_DURATION, SPLASH_DURATION) {
            @Override
            public void onTick(long millisUntilFinished) {
                // nothing
            }

            @Override
            public void onFinish() {
                mTimerFinished = true;
                openNextActivity();
            }
        };
    }

    private void openNextActivity() {
        if (mTimerFinished) {
            if (mSplashPresenter.isLoginSuccessful()) {
                openHomeActivity();
                mSplashPresenter.restoreChatSession();
            } else {
                openLoginActivity();
            }
        }
    }

    @Override
    public void login() {
        openNextActivity();
    }

    private void openLoginActivity() {
        AuthUtils.openLoginActivity(this);
    }

    private void openHomeActivity() {
        HomeActivity.start(this);
    }

}
