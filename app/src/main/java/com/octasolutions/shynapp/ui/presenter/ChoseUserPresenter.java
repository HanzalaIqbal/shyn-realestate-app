package com.octasolutions.shynapp.ui.presenter;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.UserTypeResponse;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.ChoseUserMvpView;
import com.octasolutions.shynapp.util.RxUtil;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

/**
 * @author HANZALA
 */
public class ChoseUserPresenter extends BasePresenter<ChoseUserMvpView> {

    private final DataManager dataManager;
    private CompositeSubscription subscription;

    @Inject
    public ChoseUserPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void setUserType(int i) {
        checkViewAttached();
        subscription.add(
                dataManager.setUserType(i)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleUserTypeResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );

    }

    private void handleUserTypeResponse(UserTypeResponse response) {
        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                getMvpView().onSetUserType();
            } else {
                getMvpView().displayToast(response.msg);
            }
        }
    }
}