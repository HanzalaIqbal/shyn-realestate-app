package com.octasolutions.shynapp.ui.adapter.viewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.model.ZipCodeObject;
import com.octasolutions.shynapp.event.ZipSelectEvent;
import com.octasolutions.shynapp.ui.adapter.ZipCodesAdapter;
import com.octasolutions.shynapp.util.RxUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author HANZALA
 */

public class ZipCodeItemHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_zip_code)
    TextView mZipCode;

    private ZipCodeObject mZipCodeObject;
    private ZipCodesAdapter.ZipCodeType mType;
    private Context context;

    public ZipCodeItemHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindData(ZipCodeObject object, int position, ZipCodesAdapter.ZipCodeType type) {
        mZipCodeObject = object;
        mType = type;
        context = itemView.getContext();
        mZipCode.setText(String.valueOf(mZipCodeObject.getZipCode()));
    }

    @OnClick(R.id.ll_zip_code)
    public void likePost(View view) {
            RxUtil.post(view.getContext(), new ZipSelectEvent(mZipCodeObject, mType));
    }

}