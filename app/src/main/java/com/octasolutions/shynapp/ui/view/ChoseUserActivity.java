package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.annotations.SerializedName;
import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.ChoseUserMvpView;
import com.octasolutions.shynapp.ui.presenter.ChoseUserPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author HANZALA
 */
public class ChoseUserActivity extends BaseActivity implements ChoseUserMvpView {

    @Inject
    ChoseUserPresenter mPresenter;

    @BindView(R.id.ll_motivated_seller)
    RelativeLayout llMotivatedSeller;
    @BindView(R.id.ll_cash_buyer)
    RelativeLayout llCashBuyer;
    @BindView(R.id.ll_wholesaler)
    RelativeLayout llWholesaler;
    @BindView(R.id.ll_real_estate_agent)
    RelativeLayout llRealEstateAgent;

    @BindViews({R.id.motivated_seller, R.id.cash_buyer, R.id.wholesaler, R.id.real_estate_agent})
    List<CheckBox> cbUserTypes;
//    @BindView(R.id.motivated_seller)
//    CheckBox cbMotivatedSeller;
//    @BindView(R.id.cash_buyer)
//    CheckBox cbCashBuyer;
//    @BindView(R.id.wholesaler)
//    CheckBox cbWholesaler;
//    @BindView(R.id.real_estate_agent)
//    CheckBox cbRealEstateAgent;

    @BindViews({R.id.tv_motivated_seller, R.id.tv_cash_buyer, R.id.tv_wholesaler, R.id.tv_real_estate_agent})
    List<TextView> tvUserTypes;
//    @BindView(R.id.tv_motivated_seller)
//    TextView tvMotivatedSeller;
//    @BindView(R.id.tv_cash_buyer)
//    TextView tvCashBuyer;
//    @BindView(R.id.tv_wholesaler)
//    TextView tvWholesaler;
//    @BindView(R.id.tv_real_estate_agent)
//    TextView tvRealEstateAgent;

    public static void start(Context context) {
        final Intent startIntent = new Intent(context, ChoseUserActivity.class);
        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        mPresenter.attachView(this);


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @OnClick(R.id.btn_done)
    public void onDone(View view) {
//        SetProfileActivity.start(this);
        for (int i = 0; i < cbUserTypes.size(); i++) {
            if (cbUserTypes.get(i).isChecked()) {
                mPresenter.setUserType(i + 1);
                break;
            }
        }

    }

    @Override
    public void onSetUserType() {
        SetProfileActivity.start(this);
    }

    public static enum SellerType {
        @SerializedName("1")
        MOTIVATED_SELLER,
        @SerializedName("2")
        CASH_BUYER,
        @SerializedName("3")
        WHOLESALER,
        @SerializedName("4")
        REAL_ESTATE_AGENT
    }

    @OnClick({R.id.ll_motivated_seller, R.id.ll_cash_buyer, R.id.ll_wholesaler, R.id.ll_real_estate_agent})
    public void setChecked(View view) {
        switch (view.getId()) {
            case R.id.ll_motivated_seller:
                setAsChecked(cbUserTypes.get(0), tvUserTypes.get(0));
                break;
            case R.id.ll_cash_buyer:
                setAsChecked(cbUserTypes.get(1), tvUserTypes.get(1));
                break;
            case R.id.ll_wholesaler:
                setAsChecked(cbUserTypes.get(2), tvUserTypes.get(2));
                break;
            case R.id.ll_real_estate_agent:
                setAsChecked(cbUserTypes.get(3), tvUserTypes.get(3));
                break;
        }

    }

    private void setAsChecked(CheckBox checkBox, TextView tv) {
        // uncheck all
        for (int x = 0; x < cbUserTypes.size(); x++) {
            cbUserTypes.get(x).setChecked(false);
            tvUserTypes.get(x).setTextColor(getResources().getColor(R.color.txt_color_hint));
        }
//        cbMotivatedSeller.setChecked(false);
//        tvMotivatedSeller.setTextColor(getResources().getColor(R.color.txt_color_hint));
//        cbCashBuyer.setChecked(false);
//        tvCashBuyer.setTextColor(getResources().getColor(R.color.txt_color_hint));
//        cbWholesaler.setChecked(false);
//        tvWholesaler.setTextColor(getResources().getColor(R.color.txt_color_hint));
//        cbRealEstateAgent.setChecked(false);
//        tvRealEstateAgent.setTextColor(getResources().getColor(R.color.txt_color_hint));

        // set Checked selected one
        tv.setTextColor(getResources().getColor(R.color.txt_color_selected));
        checkBox.setChecked(true);

    }

    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_choose_user_type;
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
//        setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
//        setActionBarTitle(R.string.ab_my_teams);
    }

}