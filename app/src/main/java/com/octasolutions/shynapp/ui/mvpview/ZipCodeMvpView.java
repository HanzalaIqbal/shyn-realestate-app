package com.octasolutions.shynapp.ui.mvpview;

import com.octasolutions.shynapp.data.model.CityObject;
import com.octasolutions.shynapp.data.model.PromotionObject;
import com.octasolutions.shynapp.data.model.StateObject;
import com.octasolutions.shynapp.data.model.ZipCodeObject;
import com.octasolutions.shynapp.ui.base.MvpView;

import java.util.ArrayList;

/**
 * @author HANZALA
 */

public interface ZipCodeMvpView extends MvpView {
    void onAllStatesResult(ArrayList<StateObject> states);

    void onAllCitiesResult(ArrayList<CityObject> cities);

    void onZipCodesResult(ArrayList<ZipCodeObject> zipCodes);

    void onPromotionResult(PromotionObject promotion);

    void onPurchaseZipCodesResult(String msg);
}
