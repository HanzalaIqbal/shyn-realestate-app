package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.SignUpMvpView;
import com.octasolutions.shynapp.ui.presenter.SignUpPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author HANZALA
 */
public class SignUpActivity extends BaseActivity implements SignUpMvpView {

    @Inject
    SignUpPresenter mPresenter;

    @BindView(R.id.tv_terms_and_policies)
    TextView termsAndPolicies;

    @BindView(R.id.et_first_name)
    EditText mFirstName;
    @BindView(R.id.et_middle_name)
    EditText mMiddleName;
    @BindView(R.id.et_last_name)
    EditText mLastName;
    @BindView(R.id.et_dob)
    EditText mDOB;
    @BindView(R.id.tab_gender)
    TabLayout mGender;
    @BindView(R.id.et_email)
    EditText mEmail;
    @BindView(R.id.et_password)
    EditText mPassword;
    @BindView(R.id.et_password_confirm)
    EditText mPasswordConfirm;

    public static void start(Context context) {
        context.startActivity(new Intent(context, SignUpActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        ButterKnife.bind(this);
//        initToolbar();

        mPresenter.attachView(this);

        initTextView(termsAndPolicies, getString(R.string.clicking_agree),
                getString(R.string.terms), getString(R.string.and_read),
                getString(R.string.data_policies));

    }

    private void initTextView(TextView view, final String... parms) {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                parms[0] + " ");
        spanTxt.append(parms[1]);
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                showToast(parms[1]);
            }
        }, spanTxt.length() - parms[1].length(), spanTxt.length(), 0);
        spanTxt.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD),
                spanTxt.length() - parms[1].length(), spanTxt.length(), 0);
        spanTxt.append(" " + parms[2] + " ");
//        spanTxt.setSpan(new ForegroundColorSpan(Color.BLACK), 32, spanTxt.length(), 0);
        spanTxt.append(parms[3]);
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                showToast(parms[3]);
            }
        }, spanTxt.length() - parms[3].length(), spanTxt.length(), 0);
        spanTxt.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD),
                spanTxt.length() - parms[3].length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
        view.setGravity(Gravity.CENTER_HORIZONTAL);
        view.setLinkTextColor(Color.WHITE);
    }

    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_sign_up;
    }

    @OnClick(R.id.btn_sign_up)
    public void signUp(){
        mPresenter.signUpEmail(mFirstName.getText().toString(),mMiddleName.getText().toString(),
                mLastName.getText().toString(),mDOB.getText().toString(), mGender.getSelectedTabPosition()==0? "Male":"Female",
               mEmail.getText().toString(),mPassword.getText().toString().equals(mPasswordConfirm.getText().toString())?mPassword.getText().toString():"false");
    }

    @OnClick(R.id.tv_sign_in)
    public void signIn() {
        LoginActivity.start(this);
    }

    @Override
    public void signupSuccess(String msg) {
        displayToast(msg);
        ChoseUserActivity.start(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}