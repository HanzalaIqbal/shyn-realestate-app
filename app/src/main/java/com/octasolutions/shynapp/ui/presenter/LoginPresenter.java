package com.octasolutions.shynapp.ui.presenter;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.text.TextUtils;

import com.facebook.CallbackManager;
import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.remote.request.EmailLoginRequest;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.ui.base.BasePresenter;
import com.octasolutions.shynapp.ui.mvpview.LoginMvpView;
import com.octasolutions.shynapp.util.RxUtil;
import com.octasolutions.shynapp.util.chat.ChatHelper;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class LoginPresenter extends BasePresenter<LoginMvpView> {

    private final DataManager dataManager;
    private CompositeSubscription subscription;
    private CallbackManager callbackManager;
    //    private FacebookLoginResponse facebookLoginResponse;
    private String facebookEmail;

    @Inject
    public LoginPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
        subscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.unsubscribe(subscription);
    }

    public void tryEmailLogin(String email, String password) {
        checkViewAttached();
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            getMvpView().loginFailed(R.string.empty_credentials);
            return;
        }
        final EmailLoginRequest request = new EmailLoginRequest(email, password);
        subscription.add(
                Observable.just(request)
                        .withLatestFrom(dataManager.login(request), Pair::new)
                        .compose(RxUtil::async)
                        .subscribe(
                                this::handleEmailLoginResponse,
                                throwable -> {
                                    getMvpView().networkError(throwable);
                                }
                        )
        );

    }

    private void handleEmailLoginResponse(Pair<EmailLoginRequest, LoginResponse> pair) {
        final EmailLoginRequest request = pair.first;
        final LoginResponse response = pair.second;

        if (isViewAttached()) {
            if (response.code == LoginResponse.Success.SUCCESS) {
                dataManager.userId().set(response.data.getUserId());
                dataManager.userChatID().set(response.data.getChatId());
                dataManager.userPassword().set(request.getmPassword());


                getUserByID(Integer.parseInt(response.data.getChatId()), request.getmPassword());
                getMvpView().loginSuccess();
            } else {
                // TODO: refactor this to handle different success values instead of comparing strings
                if (response.msg.equals("Invalid Email or Password")) {
                    getMvpView().loginFailed(response.msg);
                }
                getMvpView().loginFailed(response.msg);
            }
        }
    }

    private void loginQB(final QBUser user) {
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Timber.w("login Onsuccess ");
            }

            @Override
            public void onError(QBResponseException e) {
                Timber.e(e, "login error %s", e.getErrors().toArray().toString());
            }
        });
    }

    private void getUserByID(int chatID, String password) {
        List<Integer> ids = new ArrayList<>();
        ids.add(chatID);

        QBUsers.getUsersByIDs(ids, null).performAsync(new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> result, Bundle params) {
                if (result.isEmpty()) {
                    Timber.w("No user found");
                    return;
                }

                QBUser qbUser = result.get(0);
                qbUser.setPassword(password);
                loginQB(qbUser);
            }


            @Override
            public void onError(QBResponseException e) {
                Timber.w(e, "No user found");
            }
        });


    }
}
