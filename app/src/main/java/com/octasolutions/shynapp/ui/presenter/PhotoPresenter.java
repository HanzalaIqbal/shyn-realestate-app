package com.octasolutions.shynapp.ui.presenter;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.mvpview.PhotoView;
import com.octasolutions.shynapp.util.PermissionUtil;
import com.octasolutions.shynapp.util.ScalingUtilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import timber.log.Timber;

/**
 * @author Mike
 */
public abstract class PhotoPresenter<T extends PhotoView> extends PermissionsPresenter<T> {
    private static final int PICK_FROM_GALLERY = 0;
    private static final int OPEN_CAMERA = 1;
    private static final int DELETE_IMAGE = 2;
    private static final int MAX_SIZE = 2048;

    protected PhotoView mView;
    protected String mPictureBase64String;
    private int mPhotoSource;

    public PhotoPresenter() {
    }

    @Override
    public void attachView(T mvpView) {
        super.attachView(mvpView);
        mView = mvpView;
    }

    @Override
    public void detachView() {
        super.detachView();
        mView = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean allPermissionsGranted = true;
        for (int i = 0; i < permissions.length; i++) {
            String p = permissions[i];
            int grantResult = grantResults[i];
            final PermissionUtil.Permission currentPermission;
            if (p.equals(PermissionUtil.Permission.CAMERA.getPermission())) {
                currentPermission = PermissionUtil.Permission.CAMERA;
            } else if (p.equals(PermissionUtil.Permission.STORAGE.getPermission())) {
                currentPermission = PermissionUtil.Permission.STORAGE;
            } else {
                continue;
            }

            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                allPermissionsGranted = false;
                showPermissionRationaleDialog(currentPermission);
                break;
            }
        }

        if (allPermissionsGranted) {
            onPhotoSourcePicked(mPhotoSource, false);
        }
    }

    public int getPhotoSourceOptions() {
        return R.array.choose_avatar_dialog_items;
    }

    public void onPhotoSourcePicked() {
        onPhotoSourcePicked(mPhotoSource, false);
    }

    public void onPhotoSourcePicked(int option, boolean showAlertDialog) {
        mPhotoSource = option;
        switch (option) {
            case PICK_FROM_GALLERY:
                if (checkAndRequestPermission(showAlertDialog, PermissionUtil.Permission.STORAGE)) {
                    mView.openGallery();
                }
                break;
            case OPEN_CAMERA:
                if (checkAndRequestPermission(showAlertDialog, PermissionUtil.Permission.CAMERA, PermissionUtil.Permission.STORAGE)) {
                    mView.openCamera();
                }
                break;
            case DELETE_IMAGE:
                mView.setPlaceHolderImage();
                break;
            default:
                //not reachable
                break;
        }
    }

    public void onPhotoPicked(Bitmap bitmap, @NonNull Uri photoImageUri) {
        try {
            // Downsize image
            if (bitmap != null) {
                if (mPhotoSource == OPEN_CAMERA && !TextUtils.isEmpty(photoImageUri.getPath())) {
                    bitmap = rotateBitmap(bitmap, photoImageUri.getPath());
                }
                if (bitmap.getHeight() > MAX_SIZE || bitmap.getWidth() > MAX_SIZE) {
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, MAX_SIZE, MAX_SIZE);
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                mView.displayPhoto(bitmap);
                byte[] dataFiled = baos.toByteArray();
                try {
                    baos.flush();
                    baos.close();
                } catch (Exception e) {
                    Timber.e(e, "Failed to convert photo bitmap into bytes");
                }
                mPictureBase64String = Base64.encodeToString(dataFiled, Base64.NO_WRAP);
            }
        } catch (Exception e) {
            Timber.e(e, "Failed to compress photo bitmap");
        }
    }

    private Bitmap rotateBitmap(Bitmap bitmap, String filePath) throws IOException {
        ExifInterface ei = new ExifInterface(filePath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(bitmap, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(bitmap, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(bitmap, 270);
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                return bitmap;
        }
    }

    private Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    @Nullable
    public String getBase64PictureString() {
        return !TextUtils.isEmpty(mPictureBase64String) ? mPictureBase64String : null;
    }
}
