package com.octasolutions.shynapp.ui.mvpview;

import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.ui.base.MvpView;

import java.util.ArrayList;

/**
 * @author HANZALA
 */

public interface HomeMvpView extends MvpView {
    void displayPosts(ArrayList<PostObject> postObjects);

    void likePostById(long userId, long postId, PostObject.LikeStatus isLiked);

    void openLogin();
}
