package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.base.BaseActivity;

import butterknife.ButterKnife;

/**
 * @author HANZALA
 */
public class FollowersActivity extends BaseActivity {


    public static void start(Context context) {
        context.startActivity(new Intent(context, FollowersActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initToolbar();

        initView();
        initToolbar();

    }

    private void initView() {
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
        setNavigationIcon(R.drawable.ic_home);
        setActionBarTitle(R.string.ab_followers);
    }


    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_followers;
    }



//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.inbox_menu, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @OnClick({R.id.item_1,R.id.item_2,R.id.item_3,R.id.item_4,R.id.item_5,})
//    void openChatActivity() {
//        ChatActivity.start(this);
//    }
}