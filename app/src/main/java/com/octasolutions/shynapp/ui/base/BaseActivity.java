package com.octasolutions.shynapp.ui.base;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ShynApplication;
import com.octasolutions.shynapp.injection.component.ActivityComponent;
import com.octasolutions.shynapp.injection.component.DaggerActivityComponent;
import com.octasolutions.shynapp.injection.module.ActivityModule;
import com.octasolutions.shynapp.util.AuthUtils;
import com.octasolutions.shynapp.util.RxEventBus;
import com.octasolutions.shynapp.util.RxUtil;

import java.util.concurrent.atomic.AtomicLong;

import javax.inject.Inject;

import butterknife.BindView;
import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class BaseActivity extends AppCompatActivity implements ErrorMvpView  {

    private static final String KEY_ACTIVITY_ID = "KEY_ACTIVITY_ID";
    private static final AtomicLong NEXT_ID = new AtomicLong(0);
    private long mActivityId;

    @Nullable
    @BindView(R.id.appbar)
    protected AppBarLayout mAppBar;
    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @Nullable
    protected ActionBar mActionBar;

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityId = savedInstanceState != null ?
                savedInstanceState.getLong(KEY_ACTIVITY_ID) : NEXT_ID.getAndIncrement();


        setContentView(getContentLayoutResource());
    }

    public ActivityComponent activityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(ShynApplication.get(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(KEY_ACTIVITY_ID, mActivityId);
    }

    //region RxJava
    @Inject
    RxEventBus mEventBus;
    protected CompositeSubscription mSubscription;

    @CallSuper
    protected void subscribe() {
        mSubscription = new CompositeSubscription();
    }

    protected <T> void subscribeEvent(Class clazz, Action1<? super T> onNext) {
        RxUtil.subscribeEvent(mEventBus, mSubscription, clazz, onNext);
    }
    //endregion

    @Override
    protected void onResume() {
        super.onResume();
        subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        RxUtil.unsubscribe(mSubscription);
    }

    /**
     * Override if necessary to provide custom layout
     *
     * @return activity layout resource
     */
    @LayoutRes
    protected int getContentLayoutResource() {
        return R.layout.ac_login;
    }

    //region ---------------------------------------- Toolbar methods
    protected void initToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mActionBar = getSupportActionBar();
            if (mActionBar != null) {
                mActionBar.setTitle(null);
            }
        }
    }

    protected void setActionBarTitle(@StringRes int title) {
        if (mActionBar != null) {
            mActionBar.setTitle(title);
        }
    }

    protected void setActionBarTitle(String title) {
        if (mActionBar != null) {
            mActionBar.setTitle(title);
        }
    }

    protected void setNavigationIcon(@DrawableRes int icon) {
        if (mToolbar != null) {
            mToolbar.setNavigationIcon(icon);
        }
    }
    //endregion

    //region ---------------------------------------- Fragment methods

    protected void replaceFragment(Fragment fragment, int resId) {
        replaceFragment(fragment, resId, false, 0, 0, 0, 0);
    }

    public void replaceFragment(Fragment fragment, int resId, boolean addToBackStack) {
        replaceFragment(fragment, resId, addToBackStack, 0, 0, 0, 0);
    }

    public void replaceFragment(Fragment fragment, int resId, boolean addToBackStack, int enter, int exit) {
        replaceFragment(fragment, resId, addToBackStack, enter, exit, 0, 0);
    }

    public void replaceFragment(Fragment fragment, int resId, boolean addToBackStack, int enter, int exit, int popEnter, int popExit) {
        FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
        if (enter != 0 && exit != 0) {
            if (popEnter != 0 && popExit != 0) {
                tr.setCustomAnimations(enter, exit, popEnter, popExit);
            } else {
                tr.setCustomAnimations(enter, exit);
            }
        }
        tr.replace(resId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            tr.addToBackStack(fragment.getClass().getName());
        }
        tr.commit();
    }

    public void addFragment(Fragment fragment, int resId, boolean addToBackStack, int enter, int exit, int popEnter, int popExit) {
        FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
        tr.setCustomAnimations(enter, exit, popEnter, popExit);
        tr.add(resId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            tr.addToBackStack(fragment.getClass().getName());
        }
        tr.commit();
    }

    public void addFragment(Fragment fragment, int resId) {
        addFragment(fragment, resId, false);
    }

    public void addFragment(Fragment fragment, int resId, boolean addToBackStack) {
        FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
        tr.add(resId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            tr.addToBackStack(fragment.getClass().getName());
        }
        tr.commit();

    }
    //endregion

//    @IntDef({Toast.LENGTH_SHORT, Toast.LENGTH_LONG})
//    @Retention(RetentionPolicy.SOURCE)
//    public @interface Duration {
//    }

    public void showToast(CharSequence text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    public void showToast(@StringRes int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_LONG).show();
    }

    @Override
    public void networkError(Throwable throwable) {

        if (throwable instanceof HttpException) {
            switch (((HttpException) throwable).code()) {
                case 401:
                    AuthUtils.logout(ShynApplication.get(this).getComponent().dataManager());
                    AuthUtils.openLoginActivity(this);
                    displayToast(R.string.authorization_error);
                    break;
                default:
                    displayToast(R.string.network_error);
                    Timber.e(throwable,((HttpException) throwable).message());
            }
        } else {
            Timber.e(throwable,"network error");
            displayToast(R.string.network_error);
        }
    }

    @Override
    public void displayToast(String message) {
        showToast(message);
    }

    @Override
    public void displayToast(@StringRes int message) {
        showToast(message);
    }

}
