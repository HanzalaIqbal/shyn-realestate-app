package com.octasolutions.shynapp.ui.mvpview;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.octasolutions.shynapp.ui.base.MvpView;

/**
 * @author HANZALA
 */
public interface PermissionsView extends MvpView {
    boolean isPermissionGranted(@NonNull String permission);

    boolean shouldShowRequestPermissionRationale(@NonNull String permission);

    void requestPermissions(@NonNull String[] permissions, int requestCode);

    void showPermissionRationaleDialog(@StringRes int rationale);
}
