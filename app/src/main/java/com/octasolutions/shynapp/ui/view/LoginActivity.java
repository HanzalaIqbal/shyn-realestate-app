package com.octasolutions.shynapp.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.widget.EditText;

import com.octasolutions.shynapp.R;
import com.octasolutions.shynapp.ui.base.BaseActivity;
import com.octasolutions.shynapp.ui.mvpview.LoginMvpView;
import com.octasolutions.shynapp.ui.presenter.LoginPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author HANZALA
 */
public class LoginActivity extends BaseActivity implements LoginMvpView {

    @Inject
    LoginPresenter mLoginPresenter;

    @BindView(R.id.et_email)
    EditText mEmail;
    @BindView(R.id.et_password)
    EditText mPassword;

    public static void start(Context context) {
        final Intent startIntent = new Intent(context, LoginActivity.class);
        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        ButterKnife.bind(this);
//        initToolbar();

        mLoginPresenter.attachView(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.detachView();
    }

    @Override
    protected int getContentLayoutResource() {
        return R.layout.ac_login;
    }

    @OnClick(R.id.ll_sign_up)
    public void onSignUpClick() {
        SignUpActivity.start(this);
    }

    @OnClick(R.id.btn_login)
    public void onSignInClick() {
        mLoginPresenter.tryEmailLogin(mEmail.getText().toString(), mPassword.getText().toString());
    }



    @Override
    public void loginSuccess() {
        HomeActivity.start(this);
    }

    @Override
    public void loginFailed() {
        showToast(R.string.login_failed_try_again_later);
    }

    @Override
    public void loginFailed(String description) {
        showToast(description);
    }

    @Override
    public void loginFailed(@StringRes int reason) {
        showToast(reason);
    }

    @Override
    public void registerFacebook(String facebookAccessToken, String email) {

    }
}