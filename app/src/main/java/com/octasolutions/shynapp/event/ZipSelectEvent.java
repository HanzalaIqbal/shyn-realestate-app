package com.octasolutions.shynapp.event;

import com.octasolutions.shynapp.data.model.ZipCodeObject;
import com.octasolutions.shynapp.ui.adapter.ZipCodesAdapter;

/**
 * @author HANZALA
 */

public class ZipSelectEvent {
    ZipCodeObject mZipCode;
    ZipCodesAdapter.ZipCodeType mType;

    public ZipSelectEvent(ZipCodeObject zipCodeObject, ZipCodesAdapter.ZipCodeType type) {
        mZipCode = zipCodeObject;
        mType = type;
    }

    public ZipCodeObject getZipCode() {
        return mZipCode;
    }

    public ZipCodesAdapter.ZipCodeType getType() {
        return mType;
    }
}
