package com.octasolutions.shynapp.event;

/**
 * @author HANZALA
 */
public class PostLikedEvent {
    long mPostId;
    boolean mIsLiked;

    public PostLikedEvent(long postId, boolean isLiked) {
        mPostId = postId;
        mIsLiked = isLiked;
    }

    public long getPostId() {
        return mPostId;
    }

    public boolean isLiked() {
        return mIsLiked;
    }
}