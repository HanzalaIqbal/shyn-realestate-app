package com.octasolutions.shynapp.event;

/**
 * @author Mike
 */
public class OnSingleChoiceOptionSelectedEvent {

    private int mOption;

    public OnSingleChoiceOptionSelectedEvent(int option) {
        mOption = option;
    }

    public int getOption() {
        return mOption;
    }

}
