package com.octasolutions.shynapp.data.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.octasolutions.shynapp.data.model.UserObject;
import com.octasolutions.shynapp.data.remote.BaseNetworkBean;

/**
 * @author HANZALA
 */
public class LoginResponse extends BaseNetworkBean {

    @SerializedName("code")
    @Expose
    public Success code;

    @SerializedName("msg")
    @Expose
    public String msg;

    @SerializedName("data")
    @Expose
    public UserObject data;


    public enum Success {
        @SerializedName("100")
        SUCCESS,
        @SerializedName("101")
        FAILURE,
    }
}