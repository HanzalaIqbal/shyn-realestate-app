package com.octasolutions.shynapp.data.remote.request;

import com.octasolutions.shynapp.data.remote.BaseNetworkBean;

/**
 * @author HANZALA
 */

public class EmailLoginRequest extends BaseNetworkBean{
    String mEmail;
    String mPassword;

    public EmailLoginRequest(String mEmail, String mPassword) {
        this.mEmail = mEmail;
        this.mPassword = mPassword;
    }

    public String getmEmail() {
        return mEmail;
    }

    public String getmPassword() {
        return mPassword;
    }
}
