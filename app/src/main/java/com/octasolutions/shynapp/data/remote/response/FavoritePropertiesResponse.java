package com.octasolutions.shynapp.data.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.octasolutions.shynapp.data.model.PostObject;
import com.octasolutions.shynapp.data.remote.BaseNetworkBean;

import java.util.ArrayList;

/**
 * @author HANZALA
 */
public class FavoritePropertiesResponse extends BaseNetworkBean {

    @SerializedName("code")
    @Expose
    public LoginResponse.Success code;

    @SerializedName("msg")
    @Expose
    public String msg;

    @SerializedName("data")
    @Expose
    public ArrayList<PostObject> data;
}