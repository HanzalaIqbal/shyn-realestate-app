package com.octasolutions.shynapp.data.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.octasolutions.shynapp.data.model.PromotionObject;
import com.octasolutions.shynapp.data.remote.BaseNetworkBean;

/**
 * @author HANZALA
 */

public class PromotionsResponse extends BaseNetworkBean {
    @SerializedName("code")
    @Expose
    public LoginResponse.Success code;

    @SerializedName("msg")
    @Expose
    public String msg;

    @SerializedName("data")
    @Expose
    public PromotionObject promotion;
}
