package com.octasolutions.shynapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author HANZALA
 */

public class PurchasedCityObject {

    @SerializedName("cityId_FK")
    @Expose
    private long cityId;

    @SerializedName("cityName")
    @Expose
    private String cityName;

    public PurchasedCityObject(String string) {
        cityId = 0;
        cityName = string;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return cityName;
    }
}
