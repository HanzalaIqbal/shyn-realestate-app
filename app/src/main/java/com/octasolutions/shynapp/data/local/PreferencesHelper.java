package com.octasolutions.shynapp.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.f2prateek.rx.preferences.Preference;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.octasolutions.shynapp.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesHelper {

    public static final String PREF_FILE_NAME = "shyn_app_pref_file";

    private final Preference<Long> userId;
    private final Preference<String> userChatID;
    private final Preference<String> userPassword;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        RxSharedPreferences rxPreferences = RxSharedPreferences.create(preferences);

        userId = rxPreferences.getLong("user_id", 0L);
        userChatID = rxPreferences.getString("user_chat_id","");
        userPassword = rxPreferences.getString("user_password","");
    }

    public Preference<Long> getUserId() {
        return userId;
    }

    public Preference<String> getUserChatID() {
        return userChatID;
    }

    public Preference<String> getUserPassword() {
        return userPassword;
    }
}
