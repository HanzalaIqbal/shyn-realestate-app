package com.octasolutions.shynapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author HANZALA
 */
public class PostObject {

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("userId")
    @Expose
    private long userId;

    @SerializedName("houseNo")
    @Expose
    private String houseNo;

    @SerializedName("streetNo")
    @Expose
    private String streetNo;

    @SerializedName("askPrice")
    @Expose
    private String askPrice;

    @SerializedName("sqFeet")
    @Expose
    private String sqFeet;

    @SerializedName("estRepCost")
    @Expose
    private String estRepCost;

    @SerializedName("estARV")
    @Expose
    private String estARV;

    @SerializedName("fixerOrNew")
    @Expose
    private String fixerOrNew;

    @SerializedName("additionalNote")
    @Expose
    private String additionalNote;

    @SerializedName("beds")
    @Expose
    private String beds;

    @SerializedName("baths")
    @Expose
    private String baths;

    @SerializedName("created")
    @Expose
    private String created;

    @SerializedName("img1")
    @Expose
    private String img1;

    @SerializedName("img2")
    @Expose
    private String img2;

    @SerializedName("img3")
    @Expose
    private String img3;

    @SerializedName("img4")
    @Expose
    private String img4;

    @SerializedName("img5")
    @Expose
    private String img5;

    @SerializedName("cityName")
    @Expose
    private String cityName;

    @SerializedName("zipCode")
    @Expose
    private String zipCode;

    @SerializedName("isLiked")
    @Expose
    private LikeStatus isLiked;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getAskPrice() {
        return askPrice;
    }

    public void setAskPrice(String askPrice) {
        this.askPrice = askPrice;
    }

    public String getSqFeet() {
        return sqFeet;
    }

    public void setSqFeet(String sqFeet) {
        this.sqFeet = sqFeet;
    }

    public String getEstRepCost() {
        return estRepCost;
    }

    public void setEstRepCost(String estRepCost) {
        this.estRepCost = estRepCost;
    }

    public String getEstARV() {
        return estARV;
    }

    public void setEstARV(String estARV) {
        this.estARV = estARV;
    }

    public String getFixerOrNew() {
        return fixerOrNew;
    }

    public void setFixerOrNew(String fixerOrNew) {
        this.fixerOrNew = fixerOrNew;
    }

    public String getAdditionalNote() {
        return additionalNote;
    }

    public void setAdditionalNote(String additionalNote) {
        this.additionalNote = additionalNote;
    }

    public String getBeds() {
        return beds;
    }

    public void setBeds(String beds) {
        this.beds = beds;
    }

    public String getBaths() {
        return baths;
    }

    public void setBaths(String baths) {
        this.baths = baths;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getImg4() {
        return img4;
    }

    public void setImg4(String img4) {
        this.img4 = img4;
    }

    public String getImg5() {
        return img5;
    }

    public void setImg5(String img5) {
        this.img5 = img5;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public LikeStatus getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(LikeStatus isLiked) {
        this.isLiked = isLiked;
    }

    public enum LikeStatus {
        @SerializedName("0")
        UN_LIKED,
        @SerializedName("1")
        LIKED
    }
}