package com.octasolutions.shynapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author HANZALA
 */

public class ZipCodeObject {
    @SerializedName("zipCodeId")
    @Expose
    private long zipCodeId;

    @SerializedName("zipCode")
    @Expose
    private long zipCode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("status")
    @Expose
    private Purchased status;

    public long getZipCodeId() {
        return zipCodeId;
    }

    public void setZipCodeId(long zipCodeId) {
        this.zipCodeId = zipCodeId;
    }

    public long getZipCode() {
        return zipCode;
    }

    public void setZipCode(long zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String cityName) {
        this.city = cityName;
    }

    public Purchased getStatus() {
        return status;
    }

    public void setStatus(Purchased status) {
        this.status = status;
    }

    public enum Purchased {
        @SerializedName("purchased")
        @Expose
        PURCHASED,
        @SerializedName("available")
        @Expose
        AVAILABLE

    }
}
