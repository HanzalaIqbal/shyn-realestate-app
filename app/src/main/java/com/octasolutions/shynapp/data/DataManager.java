package com.octasolutions.shynapp.data;

import com.f2prateek.rx.preferences.Preference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.octasolutions.shynapp.data.local.PreferencesHelper;
import com.octasolutions.shynapp.data.remote.ShynService;
import com.octasolutions.shynapp.data.remote.request.EmailLoginRequest;
import com.octasolutions.shynapp.data.remote.request.LikePostRequest;
import com.octasolutions.shynapp.data.remote.response.AllPropertiesResponse;
import com.octasolutions.shynapp.data.remote.response.AllStatesResponse;
import com.octasolutions.shynapp.data.remote.response.CreatePostResponse;
import com.octasolutions.shynapp.data.remote.response.FavoritePropertiesResponse;
import com.octasolutions.shynapp.data.remote.response.GetCitiesResponse;
import com.octasolutions.shynapp.data.remote.response.GetPostsResponse;
import com.octasolutions.shynapp.data.remote.response.GetProfileResponse;
import com.octasolutions.shynapp.data.remote.response.LikeResponse;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.PromotionsResponse;
import com.octasolutions.shynapp.data.remote.response.PropertyDetailResponse;
import com.octasolutions.shynapp.data.remote.response.PurchaseZipCodesResponse;
import com.octasolutions.shynapp.data.remote.response.PurchasedCitiesResponse;
import com.octasolutions.shynapp.data.remote.response.SetProfileResponse;
import com.octasolutions.shynapp.data.remote.response.SignupResponse;
import com.octasolutions.shynapp.data.remote.response.UnLikeResponse;
import com.octasolutions.shynapp.data.remote.response.UserTypeResponse;
import com.octasolutions.shynapp.data.remote.response.ZipCodesResponse;
import com.octasolutions.shynapp.util.EventPosterHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.MultipartBody;
import rx.Observable;

@Singleton
public class DataManager {

    private final ShynService mShynService;
    private final PreferencesHelper mPreferencesHelper;
    private final EventPosterHelper mEventPoster;

    @Inject
    public DataManager(ShynService shynService,
                       PreferencesHelper preferencesHelper,
                       EventPosterHelper eventPosterHelper) {
        mShynService = shynService;
        mPreferencesHelper = preferencesHelper;
        mEventPoster = eventPosterHelper;
    }


    /**
     * PREFERENCES
     **/
    public Preference<Long> userId() {
        return mPreferencesHelper.getUserId();
    }

    public Preference<String> userChatID() {
        return mPreferencesHelper.getUserChatID();
    }

    public Preference<String> userPassword() {
        return mPreferencesHelper.getUserPassword();
    }

    public String gcmId() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    /**
     * END OF PREFERENCES
     **/


    public Observable<LoginResponse> login(EmailLoginRequest request) {
        return mShynService.logIn(request.getmEmail(), request.getmPassword());
    }

    public Observable<SignupResponse> signup(String firstName, String middleName, String lastName, String dob, String gender, String email, String password) {
        return mShynService.signup(firstName, middleName, lastName, dob, gender, email, password);
    }

    public Observable<AllPropertiesResponse> getAllProperties() {
        long userId = userId().get();
        return mShynService.getAllProperties(userId);
    }

    public Observable<FavoritePropertiesResponse> getFavoriteProperties() {
        long userId = userId().get();
        return mShynService.getFavoriteProperties(userId);
    }

    public Observable<LikeResponse> likePost(LikePostRequest request) {
        long userId = request.getUserId();
        long postId = request.getPostId();
        return mShynService.likePost(String.valueOf(userId), String.valueOf(postId));
    }

    public Observable<UnLikeResponse> unLikePost(LikePostRequest request) {
        long userId = request.getUserId();
        long postId = request.getPostId();
        return mShynService.unLikePost(String.valueOf(userId), String.valueOf(postId));
    }

    public Observable<PropertyDetailResponse> getPostDetail(long mPostId) {
        return mShynService.getPropertyDetail(mPostId);
    }

    public Observable<UserTypeResponse> setUserType(int sellerType) {
        long userId = userId().get();
        return mShynService.setUserType(userId, sellerType);
    }

    public Observable<SetProfileResponse> setProfile(MultipartBody.Part filePart, String displayName, String companyName, String webAddress, String homeAddress) {
        long userId = userId().get();
        return mShynService.setProfile(userId, displayName, companyName,
                webAddress, homeAddress, "1", filePart);
    }

    public Observable<AllStatesResponse> getAllStates() {
        return mShynService.getAllStates();
    }

    public Observable<GetCitiesResponse> getCities(String stateId) {
        return mShynService.getCities(stateId);
    }

    public Observable<PurchasedCitiesResponse> getPurchasedCities() {
        long userId = userId().get();
        return mShynService.getPurchasedCities(userId);
    }

    public Observable<ZipCodesResponse> getZipCodes(long cityId) {
        long userId = userId().get();
        return mShynService.getZipCodes(userId, cityId);
    }

    public Observable<PromotionsResponse> getPromotions() {
        return mShynService.getPromotions();
    }

    public Observable<GetProfileResponse> getProfile(long mUserId) {
        return mShynService.getProfile(mUserId);
    }

    public Observable<GetPostsResponse> getPosts(long mUserId) {
        return mShynService.getPosts(mUserId);
    }

    public Observable<CreatePostResponse> createPost(String houseNo, String street, long cityId,
                                                     String askingPrice, String sqFeet, String repairCost,
                                                     String avr, int isFixer, String notes, int beds,
                                                     int baths, MultipartBody.Part img1, MultipartBody.Part img2, MultipartBody.Part img3,
                                                     MultipartBody.Part img4, MultipartBody.Part img5) {
        long userId = userId().get();
        return mShynService.createPost(userId, houseNo, street, cityId, askingPrice, sqFeet, repairCost, avr,
                isFixer, notes, beds, baths, img1, img2, img3, img4, img5);
    }

    public Observable<PurchaseZipCodesResponse> purchaseZipcode(String zipCodeIDs, String paymentDetail,
                                                                int yearly, String dateTime) {
        long userId = userId().get();
        return mShynService.purchaseZipcode(userId,zipCodeIDs,paymentDetail,yearly,dateTime);
    }


//    public Observable<Void> signOut() {
//        return mDatabaseHelper.clearTables()
//                .doOnCompleted(new Action0() {
//                    @Override
//                    public void call() {
//                        mPreferencesHelper.clear();
//                        mEventPoster.postEventSafely(new BusEvent.UserSignedOut());
//                    }
//                });
//    }


}
