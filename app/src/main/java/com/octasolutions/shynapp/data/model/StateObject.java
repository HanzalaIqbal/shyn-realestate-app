package com.octasolutions.shynapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author HANZALA
 */

public class StateObject {

    @SerializedName("stateName")
    @Expose
    private String stateName;

    @SerializedName("stateId")
    @Expose
    private String stateId;

    public StateObject(String state) {
        stateName = state;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    @Override
    public String toString() {
        return stateName;
    }
}
