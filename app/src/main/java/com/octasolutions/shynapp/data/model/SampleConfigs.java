package com.octasolutions.shynapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author HANZALA
 */

public class SampleConfigs {

    @SerializedName("port")
    @Expose
    private int chatPort;

    @SerializedName("socket_timeout")
    @Expose
    private int chatSocketTimeout;

    @SerializedName("keep_alive")
    @Expose
    private boolean keepAlive;

    @SerializedName("use_tls")
    @Expose
    private boolean useTls;

    @SerializedName("auto_join")
    @Expose
    private boolean autoJoinEnabled;

    @SerializedName("auto_mark_delivered")
    @Expose
    private boolean autoMarkDelivered;

    @SerializedName("reconnection_allowed")
    @Expose
    private boolean reconnectionAllowed;

    @SerializedName("allow_listen_network")
    @Expose
    private boolean allowListenNetwork;

    public SampleConfigs(int chatPort, int chatSocketTimeout, boolean keepAlive,
                         boolean useTls, boolean autoJoinEnabled, boolean autoMarkDelivered,
                         boolean reconnectionAllowed, boolean allowListenNetwork) {
        this.chatPort = chatPort;
        this.chatSocketTimeout = chatSocketTimeout;
        this.keepAlive = keepAlive;
        this.useTls = useTls;
        this.autoJoinEnabled = autoJoinEnabled;
        this.autoMarkDelivered = autoMarkDelivered;
        this.reconnectionAllowed = reconnectionAllowed;
        this.allowListenNetwork = allowListenNetwork;
    }

    public int getChatPort() {
        return chatPort;
    }

    public void setChatPort(int chatPort) {
        this.chatPort = chatPort;
    }

    public int getChatSocketTimeout() {
        return chatSocketTimeout;
    }

    public void setChatSocketTimeout(int chatSocketTimeout) {
        this.chatSocketTimeout = chatSocketTimeout;
    }

    public boolean isKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(boolean keepAlive) {
        this.keepAlive = keepAlive;
    }

    public boolean isUseTls() {
        return useTls;
    }

    public void setUseTls(boolean useTls) {
        this.useTls = useTls;
    }

    public boolean isAutoJoinEnabled() {
        return autoJoinEnabled;
    }

    public void setAutoJoinEnabled(boolean autoJoinEnabled) {
        this.autoJoinEnabled = autoJoinEnabled;
    }

    public boolean isAutoMarkDelivered() {
        return autoMarkDelivered;
    }

    public void setAutoMarkDelivered(boolean autoMarkDelivered) {
        this.autoMarkDelivered = autoMarkDelivered;
    }

    public boolean isReconnectionAllowed() {
        return reconnectionAllowed;
    }

    public void setReconnectionAllowed(boolean reconnectionAllowed) {
        this.reconnectionAllowed = reconnectionAllowed;
    }

    public boolean isAllowListenNetwork() {
        return allowListenNetwork;
    }

    public void setAllowListenNetwork(boolean allowListenNetwork) {
        this.allowListenNetwork = allowListenNetwork;
    }
}
