package com.octasolutions.shynapp.data.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.octasolutions.shynapp.data.model.ZipCodeObject;
import com.octasolutions.shynapp.data.remote.BaseNetworkBean;

import java.util.ArrayList;

/**
 * @author HANZALA
 */

public class ZipCodesResponse extends BaseNetworkBean {

    @SerializedName("code")
    @Expose
    public LoginResponse.Success code;

    @SerializedName("msg")
    @Expose
    public String msg;

    @SerializedName("data")
    @Expose
    public ArrayList<ZipCodeObject> zipCodes;
}