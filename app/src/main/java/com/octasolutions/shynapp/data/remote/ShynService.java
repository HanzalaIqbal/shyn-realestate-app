package com.octasolutions.shynapp.data.remote;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.octasolutions.shynapp.BuildConfig;
import com.octasolutions.shynapp.data.remote.response.AllPropertiesResponse;
import com.octasolutions.shynapp.data.remote.response.AllStatesResponse;
import com.octasolutions.shynapp.data.remote.response.CreatePostResponse;
import com.octasolutions.shynapp.data.remote.response.FavoritePropertiesResponse;
import com.octasolutions.shynapp.data.remote.response.GetCitiesResponse;
import com.octasolutions.shynapp.data.remote.response.GetPostsResponse;
import com.octasolutions.shynapp.data.remote.response.GetProfileResponse;
import com.octasolutions.shynapp.data.remote.response.LikeResponse;
import com.octasolutions.shynapp.data.remote.response.LoginResponse;
import com.octasolutions.shynapp.data.remote.response.PromotionsResponse;
import com.octasolutions.shynapp.data.remote.response.PropertyDetailResponse;
import com.octasolutions.shynapp.data.remote.response.PurchaseZipCodesResponse;
import com.octasolutions.shynapp.data.remote.response.PurchasedCitiesResponse;
import com.octasolutions.shynapp.data.remote.response.SetProfileResponse;
import com.octasolutions.shynapp.data.remote.response.SignupResponse;
import com.octasolutions.shynapp.data.remote.response.UnLikeResponse;
import com.octasolutions.shynapp.data.remote.response.UserTypeResponse;
import com.octasolutions.shynapp.data.remote.response.ZipCodesResponse;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

public interface ShynService {

    String ENDPOINT = "  http://demo.octasolutions.pk/real_estate/index.php/Services/";
    String AUTH_HEADER = "Authorization";

    @FormUrlEncoded
    @POST("login")
    Observable<LoginResponse> logIn(@Field("txtUserEmail") String email, @Field("txtUserPassword") String password);

    @FormUrlEncoded
    @POST("userRegistration")
    Observable<SignupResponse> signup(@Field("txtFirstName") String firstName,
                                      @Field("txtMiddleName") String middleName,
                                      @Field("txtLastName") String lastName,
                                      @Field("txtDob") String dob,
                                      @Field("txtGendar") String gender,
                                      @Field("txtEmail") String email,
                                      @Field("txtPassword") String password);

    @FormUrlEncoded
    @POST("getAllProperties")
    Observable<AllPropertiesResponse> getAllProperties(@Field("txtUserId") long userId);

    @FormUrlEncoded
    @POST("getFavoritePosts")
    Observable<FavoritePropertiesResponse> getFavoriteProperties(@Field("txtUserId") long userId);

    @FormUrlEncoded
    @POST("getPropertyDetail")
    Observable<PropertyDetailResponse> getPropertyDetail(@Field("txtAdId") long postId);

    @FormUrlEncoded
    @POST("markFavorite")
    Observable<LikeResponse> likePost(@Field("txtUserId") String userId,
                                      @Field("txtAdId") String postId);

    @FormUrlEncoded
    @POST("markUnFavorite")
    Observable<UnLikeResponse> unLikePost(@Field("txtUserId") String userId,
                                          @Field("txtAdId") String postId);

    @FormUrlEncoded
    @POST("setUserType")
    Observable<UserTypeResponse> setUserType(@Field("txtUserId") long userId,
                                             @Field("txtUserType") int sellerType);

    @Multipart
    @POST("setMyProfile")
    Observable<SetProfileResponse> setProfile(@Part("txtUserId") long userId,
                                              @Part("txtDisplayName") String displayName,
                                              @Part("txtCompanyName") String companyName,
                                              @Part("txtWebAddress") String webAddress,
                                              @Part("txtAddress") String homeAddress,
                                              @Part("isAgent") String isAgent,
                                              @Part MultipartBody.Part filePart);

    @POST("getAllStates")
    Observable<AllStatesResponse> getAllStates();

    @FormUrlEncoded
    @POST("getAllCities")
    Observable<GetCitiesResponse> getCities(@Field("txtstateId") String stateId);

    @FormUrlEncoded
    @POST("getPurchasedCity")
    Observable<PurchasedCitiesResponse> getPurchasedCities(@Field("txtUserId") long userId);

    @FormUrlEncoded
    @POST("getZipAcctoCity")
    Observable<ZipCodesResponse> getZipCodes(@Field("txtuserId") long userId,
                                             @Field("txtcityId") long cityId);

    @GET("getPromotions")
    Observable<PromotionsResponse> getPromotions();

    @FormUrlEncoded
    @POST("purchaseZipcode")
    Observable<PurchaseZipCodesResponse> purchaseZipcode(@Field("userId") long userId,
                                                         @Field("zipCedeId") String zipCodeIDs,
                                                         @Field("paymentDetail") String paymentDetail,
                                                         @Field("isMonthlyOrYearly") int yearly,
                                                         @Field("dateTime") String dateTime);

    @FormUrlEncoded
    @POST("getMyProfile")
    Observable<GetProfileResponse> getProfile(@Field("txtUserId") long userId);

    @FormUrlEncoded
    @POST("getMyPosts")
    Observable<GetPostsResponse> getPosts(@Field("txtUserId") long userId);

    @Multipart
    @POST("postNewProperty")
    Observable<CreatePostResponse> createPost(@Part("txtUserId") long userId,
                                              @Part("txtHouseNo") String houseNo,
                                              @Part("txtStreetNo") String street,
                                              @Part("txtCityId") long cityId,
                                              @Part("txtPrice") String askingPrice,
                                              @Part("txtSqFeet") String sqFeet,
                                              @Part("txtRepairCost") String repairCost,
                                              @Part("txtARV") String avr,
                                              @Part("txtFixOrNew") int isFixer,
                                              @Part("txtNotes") String notes,
                                              @Part("txtNoBeds") int beds,
                                              @Part("txtNoBath") int baths,
                                              @Part MultipartBody.Part img1,
                                              @Part MultipartBody.Part img2,
                                              @Part MultipartBody.Part img3,
                                              @Part MultipartBody.Part img4,
                                              @Part MultipartBody.Part img5);


    /********
     * Factory class that sets up a new shyn services
     *******/
    class Factory {

        public static ShynService makeRibotService(Context context) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                    : HttpLoggingInterceptor.Level.NONE);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                    .addInterceptor(new UnauthorisedInterceptor(context))
                    .addInterceptor(interceptor)
                    .build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ShynService.ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(ShynService.class);
        }

    }

    class Util {
        // Build API authorization string from a given access token.
        public static String buildAuthorization(String accessToken) {
            return "Bearer " + accessToken;
        }
    }

    /********
     * Specific request and response models
     ********/
    class SignInRequest {
        public String googleAuthorizationCode;

        public SignInRequest(String googleAuthorizationCode) {
            this.googleAuthorizationCode = googleAuthorizationCode;
        }
    }


}
