package com.octasolutions.shynapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author HANZALA
 */

public class PromotionObject {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("price_month")
    @Expose
    private double priceMonth;
    @SerializedName("price_year")
    @Expose
    private double priceYear;
    @SerializedName("discount")
    @Expose
    private double discount;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("status")
    @Expose
    private int status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPriceMonth() {
        return priceMonth;
    }

    public void setPriceMonth(double priceMonth) {
        this.priceMonth = priceMonth;
    }

    public double getPriceYear() {
        return priceYear;
    }

    public void setPriceYear(double priceYear) {
        this.priceYear = priceYear;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
