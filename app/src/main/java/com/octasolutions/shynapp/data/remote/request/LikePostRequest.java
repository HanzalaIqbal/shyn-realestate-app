package com.octasolutions.shynapp.data.remote.request;

import com.octasolutions.shynapp.data.remote.BaseNetworkBean;

/**
 * @author HANZALA
 */
public class LikePostRequest extends BaseNetworkBean {

    long mPostId;
    long mUserId;
    boolean mIsLiked;

    public LikePostRequest(long userId, long postId, boolean isLiked) {
        mPostId = postId;
        mUserId = userId;
        mIsLiked = isLiked;
    }

    public long getPostId() {
        return mPostId;
    }

    public long getUserId() {
        return mUserId;
    }

    public boolean isLiked() {
        return mIsLiked;
    }
}