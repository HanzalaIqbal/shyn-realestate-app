package com.octasolutions.shynapp.injection.component;

import com.octasolutions.shynapp.injection.PerActivity;
import com.octasolutions.shynapp.injection.module.ActivityModule;
import com.octasolutions.shynapp.ui.view.ChoseUserActivity;
import com.octasolutions.shynapp.ui.view.FavoritesActivity;
import com.octasolutions.shynapp.ui.view.HomeActivity;
import com.octasolutions.shynapp.ui.view.LoginActivity;
import com.octasolutions.shynapp.ui.view.PostActivity;
import com.octasolutions.shynapp.ui.view.PostDetailActivity;
import com.octasolutions.shynapp.ui.view.ProfileActivity;
import com.octasolutions.shynapp.ui.view.SetProfileActivity;
import com.octasolutions.shynapp.ui.view.SignUpActivity;
import com.octasolutions.shynapp.ui.view.SplashActivity;
import com.octasolutions.shynapp.ui.view.YourPostsActivity;
import com.octasolutions.shynapp.ui.view.ZipCodePurchaseActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(LoginActivity loginActivity);

    void inject(SignUpActivity signUpActivity);

    void inject(HomeActivity homeActivity);

    void inject(SplashActivity splashActivity);

    void inject(FavoritesActivity favoritesActivity);

    void inject(PostDetailActivity postDetailActivity);

    void inject(ChoseUserActivity choseUserActivity);

    void inject(SetProfileActivity setProfileActivity);

    void inject(ZipCodePurchaseActivity zipCodePurchaseActivity);

    void inject(ProfileActivity profileActivity);

    void inject(YourPostsActivity yourPostsActivity);

    void inject(PostActivity postActivity);
}

