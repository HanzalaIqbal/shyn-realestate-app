package com.octasolutions.shynapp.injection.component;

import android.app.Application;
import android.content.Context;

import com.octasolutions.shynapp.ShynApplication;
import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.local.PreferencesHelper;
import com.octasolutions.shynapp.data.remote.ShynService;
import com.octasolutions.shynapp.injection.ApplicationContext;
import com.octasolutions.shynapp.injection.module.ApplicationModule;
import com.octasolutions.shynapp.ui.view.SingleChoiceDialog;
import com.octasolutions.shynapp.util.RxEventBus;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(ShynApplication shynApplication);
    void inject(SingleChoiceDialog singleChoiceDialog);

    @ApplicationContext Context context();
    Application application();
    ShynService ribotService();
    PreferencesHelper preferencesHelper();
    DataManager dataManager();

    RxEventBus eventBus();
}
