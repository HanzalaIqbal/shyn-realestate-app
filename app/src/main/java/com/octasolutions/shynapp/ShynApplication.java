package com.octasolutions.shynapp;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.octasolutions.shynapp.data.DataManager;
import com.octasolutions.shynapp.data.model.SampleConfigs;
import com.octasolutions.shynapp.injection.component.ApplicationComponent;
import com.octasolutions.shynapp.injection.component.DaggerApplicationComponent;
import com.octasolutions.shynapp.injection.module.ApplicationModule;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import timber.log.Timber;

public class ShynApplication extends MultiDexApplication {

    @Inject
    Bus mEventBus;
    @Inject
    DataManager mDataManager;
    ApplicationComponent mApplicationComponent;


    private static String QB_APP_ID = "38352";
    private static String QB_AUTH_KEY = "EZYA2K5zbvNxTPF";
    private static String QB_AUTH_SECRET = "ssfYPSzLFcwcn4N";
    private static String QB_ACCOUNT_KEY = "5U7i1weZaKDVQTvWqxi8";

    private static SampleConfigs chatConfigs;
    private static final int CHAT_PORT = 5223;
    private static final int CHAT_SOCKET_TIMEOUT = 300;
    private static final boolean CHAT_KEEP_ALIVE = true;
    private static final boolean CHAT_USE_TLS = true;
    private static final boolean CHAT_AUTO_JOIN_ENABLED = false;
    private static final boolean CHAT_AUTO_MARK_DELIVERED = true;
    private static final boolean CHAT_RECONNECTION_ALLOWED = true;
    private static final boolean CHAT_ALLOW_LISTEN_NETWORK = true;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.inject(this);
        mEventBus.register(this);

        initQb();
        initSampleConfigs();
    }

    private void initQb() {
        QBChatService.setDebugEnabled(true);

        QBSettings.getInstance().init(getApplicationContext(),
                QB_APP_ID,
                QB_AUTH_KEY,
                QB_AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(QB_ACCOUNT_KEY);
    }

    public static ShynApplication get(Context context) {
        return (ShynApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

    private void initSampleConfigs() {
        chatConfigs = new SampleConfigs(CHAT_PORT, CHAT_SOCKET_TIMEOUT, CHAT_KEEP_ALIVE, CHAT_USE_TLS,
                CHAT_AUTO_JOIN_ENABLED, CHAT_AUTO_MARK_DELIVERED, CHAT_RECONNECTION_ALLOWED, CHAT_ALLOW_LISTEN_NETWORK);
    }

    public static SampleConfigs getChatConfigs() {
        return chatConfigs;
    }
}

